'''@file        Lab0x03_UI.py
@brief          This program serially communicates with the NucleoSTM32.
@details        This program takes user input commands from the keyboard.
                Then sends the appropriate command to the back end program
                on the NucleoSTM32 board to either begin collecting ADC
                data or to stop gathering. Finally, it reads the data 
                provided by the board, computes it, and separates into a 
                CSV file for the user.

                Here is a snapshot of our transition diagram:
                    *  \image html ADC_Diagram.png width=800cm height=600cm
                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab3/Lab0x03_UI.py

@author         David Hernandez
@author         Sydney Lewis
@date           5/5/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''


import keyboard
import serial
from array import array
from matplotlib import pyplot
import csv


last_key = None

def kb_cb(key):
    """ @brief      Callback function which is called when a key has been pressed.
        @details    When a key is pressed on the keyboard it will call back 
                    this function. This will encode the letter and write it.
        @param      key, is the command accepted by this function.
    """
    global last_key
    last_key = key.name
    ser.write(str(last_key).encode('ascii'))


def write_csv(file,x,y):
    '''@brief      Writes a CSV file from data collected.
       @details    Takes the stripped data collected and separates them
                   into a time column and ADC output column.
       @param      file, the file name to store the data.
       @param      x, the first input to be stored in a column.
       @param      y, the second input to be stored in a column.
    '''
    n = 0
    with open('ADC_Data.csv','w',newline='') as data:
        data = csv.writer(data, delimiter = ',', quotechar = '|', quoting = csv.QUOTE_MINIMAL)
        for point in x:
            data.writerow([str(x[n]),str(y[n])])
            n += 1
    pass
    
# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb)
## @brief End data collection prematurely
#
keyboard.on_release_key("G", callback=kb_cb)
## @brief Collect encoder 1 data for 30 seconds
#


# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization

    ## Store the last input key from the Keyboard
    last_key = None
    ## @brief Verifies if a key has been pressed or not
    #
    dataCollect = []
    ## @brief All data read collected in this empty array
    #
    n = 0
    ## @brief Initialized the loop count for data collection
    #
    times = array('f', [])
    ## @brief Collected input data placed in this empty array
    #
    ADC = array('f', [])
    ## @brief Collected output data placed in this empty array
    #
    voltages = array('f', [])
    ## @brief Collected output data placed in this empty array
    #

    ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
    ## @brief Opens serial port to communicate with Nucleo

    print('Press G to collect data:\r\nPress S to STOP anytime:')


    while True:
        try:

            ## Provides the user feedback when the appropriate command is pressed
            if last_key is not None:
                print("You pressed " + last_key)
                ser.write(str(last_key).encode('ascii'))
                last_key = None


            ## Store the data sent from the Nucleo
            value = ser.readline().decode()
            if value == 'Complete':
                ser.close()
                break


            ## Removes spaces
            elif value == '':
                pass


            ## Strips unecessary info stripped for integers
            else:
                value = value.strip() # strip the value from Nucleo of white space
                dataCollect.append(value) # store it in dataReceived array


        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed, program ended. Goodbye')
            break


## Splits the developed data into two columns in order to plot
for i in range(len(dataCollect)):
        dataStrip = dataCollect[i]
        dataSplit = dataStrip.split(',')
        times.append(float(dataSplit[0]))
        ADC.append(float(dataSplit[1]))
        

for j in range(len(ADC)):
    vdd = 3.3
    ## @brief Source voltage from the NucleoSTM32
    #
    volts = 3.3*ADC[j]/4095
    voltages.append(float(volts))



print('Data Collection Complete')


# ##@brief Output of equation using arrayed time
# #

## Plot collected data
pyplot.figure()
pyplot.plot(times,voltages)
pyplot.xlabel('Time Elapsed [us]')
pyplot.ylabel('Voltage [V]')
pyplot.title('Voltage vs Time')
pyplot.xlim([1000, 15000])



try:
    ## Send data to csv file
    f = open('ADC_Data.csv', "x")
except:
    pass
write_csv('ADC_Data.csv',times,voltages)
print('Data Collected and Published')
# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
var classDRV8847_1_1DRV8847 =
[
    [ "__init__", "classDRV8847_1_1DRV8847.html#a22751a8a09a4a76660b451ef5b639746", null ],
    [ "channel", "classDRV8847_1_1DRV8847.html#aabcb8283746166b3d09e904d7979f512", null ],
    [ "clear_fault", "classDRV8847_1_1DRV8847.html#ab9d8538134c831ce274f4ce0feb12abb", null ],
    [ "disable", "classDRV8847_1_1DRV8847.html#acd9dbef9212b3014eab18a57a6e0f13a", null ],
    [ "enable", "classDRV8847_1_1DRV8847.html#a57adaca9549fa5935979ff8f0bcdda13", null ],
    [ "fault_CB", "classDRV8847_1_1DRV8847.html#a0384bfd51cf17c859084daadd848544e", null ],
    [ "debug", "classDRV8847_1_1DRV8847.html#ac9d4d0dceec1a36fb6ab8079790aec80", null ],
    [ "INx_pin", "classDRV8847_1_1DRV8847.html#aacc882886ae1039b3bd5eff621c38218", null ],
    [ "INxy_timer", "classDRV8847_1_1DRV8847.html#afed76ea12d35558221bec5114b73a2ed", null ],
    [ "INy_pin", "classDRV8847_1_1DRV8847.html#ae1eb5a22788a30d091247908dd48e5da", null ],
    [ "mot_fault_flag", "classDRV8847_1_1DRV8847.html#a43cdb2a43f098ab6c265791923aaec28", null ],
    [ "nFAULT_pin", "classDRV8847_1_1DRV8847.html#a93c0792671a685f003e3441cc7256bb9", null ],
    [ "nSLEEP_pin", "classDRV8847_1_1DRV8847.html#afd3c7a2a00c1a564ddab45829f1c1192", null ],
    [ "primary", "classDRV8847_1_1DRV8847.html#af8f78bfa5e3abacbbdbcbc09aeca3673", null ]
];
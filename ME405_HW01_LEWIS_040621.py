# HW # 1 - Vending Machine Change Calculator
# Written By: Sydney Lewis
# Date: 04/06/21
# Class: ME 405-01
# Instructions: Enter the price and payment at the bottom of the script.
# The price should be in cents, the payment should be entered as a collection.
# The output "change" will show the amount of money returned in the same
# format as the payment: 
# [pennies, nickles, dimes, quarters, dollars, fives, tens, twenties].
# The script is designed to generate the fewest denominations of change as
# possible. The variable "number" returns payment-price in cents to check the 
# result of the "change" vector.
# The payments larger than $40 will not be optimized for change.

def getChange(price, payment):
   result = 8*[0]
   cents = int(payment[0]) + 5*int(payment[1]) + 10*int(payment[2]) + 25*int(payment[3]) + 100*int(payment[4]) + 500*int(payment[5]) + 1000*int(payment[6]) + 2000*int(payment[7])
   result[0] = cents - price
   if result[0]>=2000:
       result[0] = int(result[0]) - 2000
       result[7] = 1
   if result[0]>=1000:
       result[0]= int(result[0]) - 1000
       result[6] = 1
   if result[0]>= 500:
       result[0]= int(result[0]) - 500
       result[5] = 1
   if result[0]>= 400:
       result[0]= int(result[0])-400
       result[4]= 4
   if result[0]>= 300:
       result[0]= int(result[0]) -300
       result[4]= 3
   if result[0]>= 200:
       result[0]= int(result[0]) - 200
       result[4]= int(result[4]) + 2
   if result[0]>=100:
      result[0] = int(result[0]) - 100
      result[4] = 1
   if result[0]>=75:
      result[0] = int(result[0]) - 75
      result[3] = 3
   if result[0]>=50:
      result[0] = int(result[0]) - 50
      result[3] = int(result[3]) + 2
   if result[0]>=25:
      result[0] = int(result[0]) - 25
      result[3] = int(result[3]) + 1
   if result[0]>=20:
      result[0] = int(result[0]) - 20
      result[2] = 2
   if result[0]>=10:
      result[0] = int(result[0]) - 10
      result[2] = int(result[2]) + 1
   if result[0]>=5:
      result[0] = int(result[0]) - 5
      result[1] = 1
   if result[0]>= 0:
      return result
   if result[0]< 0:
      result = [0]
      return result
                        
# Enter price and payment here. 
# Original price set to $2, payment set to $21.53.                       
price = 100
payment = [3,0,0,2,1,0,0,1]

# Variables used to check "change" vector.
cents = int(payment[0]) + 5*int(payment[1]) + 10*int(payment[2]) + 25*int(payment[3]) + 100*int(payment[4]) + 500*int(payment[5]) + 1000*int(payment[6]) + 2000*int(payment[7])
number = cents-price

# Vending machine change.
change = getChange(price,payment)

''' @file       UI_front.py
    @brief      User interface that serially communicates with back end programs.
    @details    Sends information back and forth between Nucleo and laptop
    @author     David Hernandez
    @author     Sydney Lewis
    @date       6/8/21
    @copyright  2020-2021 Hernandez and Lewis Inc.
'''

import keyboard
import serial
from array import array
import csv
import matplotlib.pyplot as plt




def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name


def write_csv(file,x,y,z):
    ''' @brief writes a csv file from data collected
    '''
    n = 0
    with open('Lab0xffdata.csv','w',newline='') as data:
        data = csv.writer(data, delimiter = ',', quotechar = '|', quoting = csv.QUOTE_MINIMAL)
        for point in x:
            data.writerow([str(x[n]),str(y[n]),str(z[n])])
            n += 1
    pass


# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb)
## @brief End data collection prematurely
#
keyboard.on_release_key("G", callback=kb_cb)
## @brief Collect encoder 1 data for 30 seconds
#
keyboard.on_release_key("Z", callback=kb_cb)
## @brief Zero the encoder 1 position
#
keyboard.on_release_key("P", callback=kb_cb)
## @brief Print out the encoder 1 position
#
keyboard.on_release_key("D", callback=kb_cb)
## @brief Print out the encoder 1 delta
#
keyboard.on_release_key("A", callback=kb_cb)
## @brief Print out the motor alpha gain position
#
keyboard.on_release_key("B", callback=kb_cb)
## @brief Print out the motor beta gain velocity
#
keyboard.on_release_key("U", callback=kb_cb)
## @brief Set out the motor alpha gain position
#
keyboard.on_release_key("I", callback=kb_cb)
## @brief Set out the motor beta gain velocity
#
keyboard.on_release_key("F", callback=kb_cb)
## @brief Set out the motor beta gain velocity
#

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization

    ## Store the last input key from the Keyboard
    last_key = None
    ## @brief Verifies if a key has been pressed or not
    #
    dataCollect = []
    ## @brief All data read collected in this empty array
    #
    n = 0
    ## @brief Initialized the loop count for data collection
    #
    times = array('f', [])
    ## @brief Collected input data placed in this empty array
    #
    values = array('f', [])
    ## @brief Collected output data placed in this empty array
    #
    position = array('f', [])
    ## @brief Collected output data placed in this empty array
    #
    ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)
    ## @brief Opens serial port to communicate with Nucleo
    #
    print('Press G to collect data:\r\nPress S to STOP anytime:\r\nPress Z to ZERO encoder position'
          '\r\nPress P to PRINT out encoder position:\r\nPress D to PRINT out encoder delta:'
          '\r\nPress M to PRINT out motor speed:\r\nPress A to GET alpha position gain:'
              '\r\nPress B to GET beta velocity gain''\r\nPress U to SET alpha position gain'
              '\r\nPress I to SET beta velocity gain''\r\nPRess F to FIX board')


    while True:
        try:

            ## Provides the user feedback when the appropriate command is pressed
            if last_key is not None:
                print("You pressed " + last_key)
                ser.write(str(last_key).encode('ascii'))
                last_key = None


            ## Store the data sent from the Nucleo
            value = ser.readline().decode()
            if value == 'Complete':
                ser.close()
                break


            ## Removes spaces
            elif value == '':
                pass


            ## Strips unecessary info stripped for integers
            else:
                value = value.strip() # strip the value from Nucleo of white space
                dataCollect.append(value) # store it in dataReceived array


        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed, program ended. Goodbye')
            break


#print('value = '+str(value))
#('data = '+str(data))
#print('datacollect = ' +str(datacollect))
## Splits the developed data into two columns in order to plot
for i in range(len(dataCollect)):
        dataStrip = dataCollect[i]
        dataSplit = dataStrip.split(',')
        times.append(float(dataSplit[0]))
        values.append(float(dataSplit[1]))
        position.append(float(dataSplit[2]))


print('Data Collection Complete')

## Plot collected data
fig, axs = plt.subplots(2)
fig.suptitle('alpha gain = 0.85, beta gain = 0.005')
plt.xlabel("TIME [s]")
axs[0].set_ylabel('Speed [DEG/SEC]')
axs[0].plot(times, values)
plt.ylabel("Position [DEG]")
axs[1].plot(times, position)

## Places collected data into a csv
try:
    ## Send data to csv file
    f = open('Lab0xffdata.csv', "x")
except:
    pass
write_csv('Lab0xffdata.csv',times,values,position)

print('Data collected and published')

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
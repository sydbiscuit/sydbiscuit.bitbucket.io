## \page page5 Lab0xFF: Term Project
#  \tableofcontents
#  The final laboratory was the culmination of every facet learned in the ME405 course. This incorporated the skills
#  of creating multiple drivers, tasks, user interface, and a main task to run them all simultaneously. The purpose was to
#  develop a program to autonomously and continuously keep a ball balanced on a motorized platform. The development and 
#  calculations of this project can be seen in \ref page6 and \ref page7. Each specific driver and task are described below 
#  in more details.
#
#  Equipment Used:
#  * 2 - USB-B mini cables
#  * 1 - Laptop
#  * 1 - AC-DC Power Adapter, 12V/5A
#  * 1 - NucleoSTM32 MCU
#  * 1 - Mechatronics Base Unit
#  * 2 - Motor Assemblies
#  * 1 - Top Panel Assembly
#  * 1 - Resistive Touch Panel
#  * 2 - Encoder Cables
#  * 1 - U-Joint Assembly
#  * 2 - Motor Arm/Push Road Assemblies
#  * 1 - BN0055 Breakout w/ Cable
#  * 1 - FPC Touch Breakout w/ Cable
#  * 1 - Rubberized Steel Ball
#  * 1 - Hantek Oscilloscope for Debugging
#
#  The images below are the official set up of the system model (Different Angles for viewability)
#     \image html FrontView.jpg "Figure 1. System Model Front View" width=600cm height=800cm
#     \image html RearView.jpg "Figure 2. System Model Rear View" width=600cm height=800cm
#
#  The task diagram representing the theoretical set up for each driver and tasker.
#
#     \image html TaskDiagram.png "Figure 3. System Model Task Diagram" width=600cm height=400cm
#
# \section sec7 System Model Term Project
#  \ref Lab0xFF contains the following subsections: 
#  * TouchDriver
#  * TouchTask
#  * DRV8847
#  * MotorTask
#  * EncoderDriver
#  * EncoderTask
#  * BNO055
#  * bnoTEST
#  * Closed Loop Driver
#  * Controller Task
#  * Data Collect Task
#  * UI Back Task
#  * UI Front Task
#  * Shares
#  * Main
#
# \subsection subsection18 TouchDriver
#  This driver interacts with the Nucleo STM32 microcontroller allowing objects of a
#  resistive touch panel to be created. The purpose was to allow interaction with the
#  provided resistive touch panel (RTP), which was required for the term project.
#  This driver allowed the capabilities of measuring the position and velocity of the rubberized 
#  steel ball. To do this, the driver takes negative and positive terminals for the predetermined
#  X and Y axes on the RTP. By using voltage division and the analog to digital converter,
#  the driver grounds the negative terminal for the specific axis the user is measuring and 
#  applies 3.3V to the positive terminal, while floating the remaining terminals. This provides a 
#  specific distance that is returned to the user.  To verify if contact is made with the RTP, the 
#  driver considers if there is voltage being pushed; if there is, it is immediately recognized as contact
#  with the panel.  An x,y, and z scan method was develop to individually measure each axis location or if 
#  contact is occuring. A settling period and filter were developed by oversampling and 
#  taking the average, respectively. An overall scan method was also created to allow
#  the user to measure all 3 components at once. In addition, a calibration code was added to 
#  which considered the voltage reading from a designated center, 88 and 50, for the x and
#  y axes, respectively. It is important to note that works from Peter Hinch, retired hardware
#  and firmware developer, was used for the filter and settling period. 
#  Files can be found at \ref TouchDriver.py, \ref avg.py, and \ref fir.py
#
#  The following image is for the breakout board used for the RTP:
#     \image html RTPbreakout.jpg "Figure 4. FPC Touch Breakout with Cable" width=300cm height=400cm
#
# \subsection subsection19 TouchTask
#  This task called on the \ref TouchDriver and specified the appropriate RTP object to be used.
#  Using the following pin set up: ym=PA0, xm=PA1, yp=PA6, xp=PA7, where the 'm' subscripts 
#  associate to the negative terminal on the breakout board for the appropriate axis, and the
#  'p' subscripts for the positive terminal. This task allows the user to test, calibrate, and
#  collect necessary positional values. File can be found at \ref TouchPanelTask.py.
#  The task then collected the positional values in a tuple for x,y,and z and returned the 
#  appropriate values for each. There were two conditions that had to be met and those were:
#
#  * That it must be able to read from a single channel in less than 500us  
#  * All three channels in less than 1500us 
#
#  This was to guarantee that the driver worked cooperatively with the other code.
#  Benchmark tests were performed to ensure the conditions were met. These can be seen in the 
#  images below.
#
#  The following image was the benchmark for a single channel:
#     \image html singleChannel.jpg "Figure 5. RTP Single Channel Benchmark" width=300cm height=400cm
#
#  The following image was the benchmark for all three channels:
#     \image html allChannel.jpg "Figure 6. RTP Three Channel Benchmark" width=300cm height=400cm
#
# \subsection subsection20 DRV8847
#  This file contains the class \ref DRV8847.py to drive the motors balancing the platform.
#  This class allows the user to configure the pins, timer, and channel
#  to an H-Bridge motor. The user can then create motor objects to test any motor.
#  This class uses four pin objects, two for the timer channel variables, and
#  two for each motor configured and timer object. Using these it is 
#  possible to configure a sleep pin and a fault pin. The driver software ensures the 
#  given duty command cannot exceed +/- 100, otherwise it reads the input PWM percentage 
#  and sends it to the correct channel to control motor direction. The driver also has the 
#  ability to disable motor motion if a fault occurs. Both group members made a motor driver
#  for this assignment. This driver was utilized in the Motor and Encoder Test video linked
#  in \ref subsection23. The final driver that was utilized in the controller and \ref subsection33 
#  can be seen here: \ref MotorDriver.py
#
# \subsection subsection21 MotorTask
#  The user can create motor objects using \ref MotorTask.py to test motors by configuring the pins, 
#  timer, and channel. After doing so it is possible to test any motor by
#  continuously running the motor, testing the fault, motion or by time.
#  This task was modified to run with the main tasker allowing it to control
#  all motor operations and adjust the motor PWM output as needed to maintain the ball
#  in the center of the platform. These motors are not ideal, thus requiring
#  a unique duty cycle for each to overcome their individual static friction.
#  A video, link posted below, has been made to show a benchmark test for 
#  each one of the motors with the required duty cycle for each. It also
#  displays the fault method at work in the case that there were a fault
#  with the motors.
# 
#  This link will take you to a google drive stored video for the required duty cycles to
#  overcome static friction and the fault method at work.
#
#  https://drive.google.com/file/d/1BFjKUP4uEZPgg1H8Ug0QG-MdE77Gqxpz/view?usp=sharing
#
# \subsection subsection22 EncoderDriver
#  This file contains the class \ref encoderDriver.py to read from the encoders attached to the motors.
#  This class uses a timer object to sample encoders at a specified period of 0xFFFF. The period
#  can be specified to determine the max count before overflow. A prescaler can be specified to set the number 
#  of encoder ticks equal to one count. To set the encoder ticks and timer counts equal, the prescaler 
#  is set to '0' in the timer constructor. In addition to encoder ticks, time is measured allowing angular
#  velocity to be calculated as well.
#
# \subsection subsection23 EncoderTask
#  This file contains the task \ref encoderTask.py used with the \ref subsection22 to read from the encoders attached to the motors.
#  The task specifies the pins and timers to use with each encoder, then reads from the encoders indefinitely. 
#  The EncoderTask was written to incorporate shares.variable.get() and shares.variable.put() statements in order 
#  to share data between tasks. The video below shows the encoderTask file being used in conjunction with the MotorTask 
#  file through the scheduler. This confirms the encoder and motor drivers are operational and can be used simultaneously. 
#
#  https://youtu.be/YpYT6knJONc
# 
# \subsection subsection24 BNO055
#  This file contains the \ref BNO055.py driver to control the IMU sensor attached to the platform.
#  The BNO055 data sheet was consulted to identify the correct register addresses and data allowing
#  control of the IMU. The first method verifies the sensor is connected to the I2C object specified 
#  by the user. The second method sets the mode of the IMU. The default is set to nine degrees of freedom (NDOF),
#  allowing access to all three sensors onboard the IMU: the accelerometer, magnetometer, and gyroscope. The third
#  method checks calibration status by reading from the calibration registers. The fourth method reads the Euler
#  angles of yaw, pitch, and roll. The fifth method reads the angular velocity in the x,y, and z coordinates. The 
#  remaining methods allow the user to call individual readings after the Euler() and omega() methods have been used.
#  On the left of Figure 7 shows the original sensor attached to the board with unsoldered connections. On the right
#  the BNO055 has been soldered and wired.
#  
#  \image html BNOsetup.jpg "Figure 7. BNO Sensor Before and After Soldering" width=600cm height=300cm
#
#  Once the connections were established, the BNO was connected to the Nucleo's pins located on the CN5 and CN6 rows as
#  shown in Figure 8.
#
#  \image html BNOnucleo.jpg "Figure 8. BNO Connections to the Nucleo" width=600cm height=400cm
#  
# \subsection subsection25 bnoTEST
#  This file contains the \ref bnoTEST.py used with the \ref BNO055.py. The file calls on the BNO methods to initialize,
#  check connection, set the operation mode, check calibration status, and read from the Euler and Omega registers. 
#
#  The following video shows the bnoTEST file running as a mainscript confirming that the BNO055 driver is operational.
#  The bnoTask was not implemented in the controller due to time limitations and added complexity; however, it could be used
#  for error checking and additional accuracy during balancing operation.
#
#  https://youtu.be/B8x2G9iIB38
#
# \subsection subsection26 Closed Loop Driver
#  This class takes the gain value-end calculation from the controller task,
#  and applies it to the defined calculation within the class to produce a
#  proper power output. It considers the motor's: resistance, voltage output, 
#  torque constant, and gear ratio. This power output, or duty cycle, is 
#  then returned to the controller task to be sent to the motor.
#  File can be found at \ref closedLoop.py
#
# \subsection subsection27 Controller Task
#  This task takes the previously defined gains from the homework solutions
#  discussed above, and dot products them with the following state vector:
#
#  [x, theta_y, x_dot, theta_y_dot]
#
#  where x is the RTP position in meters, theta_y is the encoder position in radians,
#  x_dot is the RTP velocity in meters per second, and theta_y_dot is the encoder 
#  angular velocity in radians per second. These are multiplied to the gains and 
#  pushed to the Cloosed Loop class which produces the duty cycle for the motors.
#  The goal is to keep the ball balanced on the board, so the RTP needs to constantly
#  correct the ball position by sending values that to the controller task to center
#  the ball.
#  File can be found at \ref controllerTask.py
#
# \subsection subsection28 UI Back Task
#  This back end task serves to communicate serially with the user interface
#  front end. Its purpose is simple, interpret any commands from the user through
#  the UI Front Task, and produce the desired results. It takes serial ASCII integers
#  and switches states, which are shared between all tasks via the shares file.
#  This allows for smooth transition between commands to all files.
#  File can be found at \ref backEndTask.py
#
# \subsection subsection29 UI Front Task
#  This front end UI is responsible for interaction between the user and the program. 
#  Using serial communication, encrypted values are sent to the back end UI to get an
#  outcome. The list of possible functions are presented to the user and they simply
#  have to press a key to send the command to the back end UI, which transitions 
#  states within all tasks to produce the desired outcome.  This interface includes commands that allow 
#  the user to begin balancing, clear a motor fault, or start data collection.
#  File can be found at \ref UI_front.py
#
# \subsection subsection30 Shares
#  This file is akin to a hub for variables. It stores all variables, which then
#  allows all tasks to call on any variable required to complete the request.
#  File can be found at \ref shares.py
#
# \subsection subsection31 Main
#  This file is similar to an engine for tasks. It fires tasks continuously and harmoniously 
#  to avoid any data corruption or interruption. It is easy to have tasks 
#  overlap one another, which causes severe system issues or system freezing.
#  It is crucial to have proper period and priority designations so that 
#  they fire off nearly instantaneously without affecting one another.
#  File can be found at \ref main.py
#
# \subsection subsection32 Data Collect Task
#  This task has the sole purpose of collecting the RTP position, RTP velocity,
#  encoder position, encoder velocity, and time ran for the duration of the tests.
#  It compiles the data into a comma separated variable (CSV) file, which can
#  be used to create plots or anything you would do in excel.
#  The file can be found at \ref dataCollectTask.py
#
# \subsection subsection33 Final Results
#  The culmination of the drivers and tasks above resulted in an autonomous ball 
#  and platform balancing robot. When the ball is placed directly in the center, 
#  the controller operates as expected with extremely small changes in RTP and 
#  encoder measurements as shown by the data collected in Figure 9.
#
# \image html RTPreadings.png "Figure 9. RTP Readings" width=600cm height=400cm
#
#  When the ball is slightly displaced, the controller jerks aggressively and 
#  commands a strong PWM signal to the motor until interrupted by the user, as shown by 
#  the encoder readings in Figure 10.
#
# \image html encoderreadings.png "Figure 10. Encoder Readings" width=600cm height=400cm
# 
# This aggresive jerk is likely due to the gains calculated in the modeling assignment: K = [-5.6397, -1.5532, -1.5159, -0.1299]].
# Multiplying the displacement by over 5 times is likely too sharp of a gain and could potentially be reduced for better performance.
# However, as shown at the beginning of the video linked below, as long as the ball starts off centered, the balancing robot works great!
#
#  https://youtu.be/f_p1HP4jAOo
#
# Although not entirely desirable results, the video does demonstrate all the drivers working together at a set period and priority level. 
# The extension of the arm seemed to be in the opposite direction of what is desirable, indicating the encoder readings may need to switch 
# signs in the software. Overall, this video shows 18 files of Python code being used cooperatively to read from the touch panel, encoders, 
# motors, and develop control algorithms to synthesize all the data and output it to a data collection task. We may not have made the perfect 
# autonomous balancing robot....but maybe we can use it as an autonomous ball launcher!
#
# \image html ball_launch.png "Figure 11. Autonomous Ball Launching Robot" width=600cm height=600cm
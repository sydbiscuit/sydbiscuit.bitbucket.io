/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Sydney Lewis's Portfolio", "index.html#sec_port", null ],
    [ "What is ME405: Mechatronics", "index.html#intro_sec", [
      [ "How the Knowledge was Implemented", "index.html#how_sec", null ],
      [ "Tools and Equipment", "index.html#tools_sec", null ],
      [ "Modules", "index.html#mod_sec", null ]
    ] ],
    [ "Laboratories", "labs.html", [
      [ "Lab0x01 Documentation", "labs.html#sec_lab1", null ],
      [ "Lab0x02 Documentation", "labs.html#sec_lab2", null ],
      [ "Lab0x03 Documentation", "labs.html#sec_lab3", null ],
      [ "Lab0x04 Documentation", "labs.html#sec_lab4", null ],
      [ "Lab0xFF Documentation", "labs.html#sec_lab0xFF", null ]
    ] ],
    [ "Homeworks", "hw.html", [
      [ "HW0x01 Documentation", "hw.html#sec_hw1", null ],
      [ "HW0x02 Documentation", "hw.html#sec_hw2", null ],
      [ "HW0x03 Documentation", "hw.html#sec_hw3", null ],
      [ "HW0x04 Documentation", "hw.html#sec_hw4", null ],
      [ "HW0x05 Documentation", "hw.html#sec_hw5", null ]
    ] ],
    [ "Lab0x01", "page1.html", [
      [ "Vendotron: The Simulated Vending Machine", "page1.html#sec1", [
        [ "The Incredible Lab0x01.py", "page1.html#subsection1", null ]
      ] ]
    ] ],
    [ "Lab0x02", "page2.html", [
      [ "Reaction Time: Comparisons", "page2.html#sec2", [
        [ "Reaction with Utime Lab0x02A.py", "page2.html#subsection2", null ],
        [ "Reaction with Output Compare and Input Capture Lab0x02B.py", "page2.html#subsection3", null ],
        [ "Result Comparisons", "page2.html#subsection4", null ]
      ] ]
    ] ],
    [ "Lab0x03 ADC Circuit", "page3.html", [
      [ "Analog Digital Converter (ADC)", "page3.html#sec3", [
        [ "ADC UI Lab0x03_UI.py", "page3.html#subsection10", null ],
        [ "ADC Main Lab0x03_main.py", "page3.html#subsection11", null ]
      ] ]
    ] ],
    [ "Lab0x04: Hot or Not?", "page4.html", [
      [ "Hot or Not?", "page4.html#sec4", [
        [ "Code", "page4.html#subsection12", null ],
        [ "Results", "page4.html#subsection13", null ]
      ] ]
    ] ],
    [ "Lab0xFF: Term Project", "page5.html", [
      [ "System Model Term Project", "page5.html#sec7", [
        [ "TouchDriver", "page5.html#subsection18", null ],
        [ "TouchTask", "page5.html#subsection19", null ],
        [ "DRV8847", "page5.html#subsection20", null ],
        [ "MotorTask", "page5.html#subsection21", null ],
        [ "EncoderDriver", "page5.html#subsection22", null ],
        [ "EncoderTask", "page5.html#subsection23", null ],
        [ "BNO055", "page5.html#subsection24", null ],
        [ "bnoTEST", "page5.html#subsection25", null ],
        [ "Closed Loop Driver", "page5.html#subsection26", null ],
        [ "Controller Task", "page5.html#subsection27", null ],
        [ "UI Back Task", "page5.html#subsection28", null ],
        [ "UI Front Task", "page5.html#subsection29", null ],
        [ "Shares", "page5.html#subsection30", null ],
        [ "Main", "page5.html#subsection31", null ],
        [ "Data Collect Task", "page5.html#subsection32", null ],
        [ "Final Results", "page5.html#subsection33", null ]
      ] ]
    ] ],
    [ "HW0x02 System Modeling", "page6.html", [
      [ "Analysis", "page6.html#sec", [
        [ "Schematic", "page6.html#subsection5", null ],
        [ "Ball Kinematics", "page6.html#subsection6", null ],
        [ "Motor Torque Relationship", "page6.html#subsection7", null ],
        [ "Equations of Motion", "page6.html#subsection8", null ],
        [ "Unknowns Isolation\\Matrices manipulation", "page6.html#subsection9", null ]
      ] ]
    ] ],
    [ "HW0x04 System Model Simulation", "page7.html", [
      [ "System Model Simulation", "page7.html#sec5", [
        [ "MATLAB", "page7.html#subsection14", null ]
      ] ]
    ] ],
    [ "HW0x05: Full State Feedback", "page8.html", [
      [ "New Gains", "page8.html#sec6", [
        [ "Eigenvalues", "page8.html#subsection15", null ],
        [ "Characteristic Polynomial Selection", "page8.html#subsection16", null ],
        [ "Gain Results", "page8.html#subsection17", null ]
      ] ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"TouchDriver_8py.html#acdacbd83d4bdf237e411f3a6388fbaaf",
"classcotask_1_1TaskList.html#a288413cdeddf60664542a92ce201200a",
"shares_8py.html#ae53517fcf4463ec229ed9638d71c6b3d"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
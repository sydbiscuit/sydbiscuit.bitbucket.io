var classDRV8847_1_1DRV8847__channel =
[
    [ "__init__", "classDRV8847_1_1DRV8847__channel.html#a820af51a03d12e063df34522b355e3b5", null ],
    [ "set_duty", "classDRV8847_1_1DRV8847__channel.html#a0f1367e92f08264c4ee3d0338a2802a4", null ],
    [ "set_time", "classDRV8847_1_1DRV8847__channel.html#aaad62cb356d8256d49ab0024f0c822e6", null ],
    [ "debug", "classDRV8847_1_1DRV8847__channel.html#ac7072ba44625640682dfbd75a719b716", null ],
    [ "duty", "classDRV8847_1_1DRV8847__channel.html#a111820207a0e800f1834d048ffe06f76", null ],
    [ "INx_pin", "classDRV8847_1_1DRV8847__channel.html#a0c0007fc68f9af487c41a1c79e22c3fc", null ],
    [ "INxy_timer", "classDRV8847_1_1DRV8847__channel.html#a45734fa6b9c458ef1979ed0b87938f09", null ],
    [ "INy_pin", "classDRV8847_1_1DRV8847__channel.html#a384d24951ecc5a914edc81f07a3f1952", null ],
    [ "tch1", "classDRV8847_1_1DRV8847__channel.html#ab39ca999da1b7a93d73d1eaf7386375e", null ],
    [ "tch2", "classDRV8847_1_1DRV8847__channel.html#a4a2150910f6aee32a41b6256e4407103", null ]
];
var Lab0x03__UI_8py =
[
    [ "kb_cb", "Lab0x03__UI_8py.html#a779c6658c226efa5b1cbf36b7f185271", null ],
    [ "write_csv", "Lab0x03__UI_8py.html#abf903f6f7873220b29d8b40c9cc6f4db", null ],
    [ "ADC", "Lab0x03__UI_8py.html#a7cafd15c801a7e5c3ec9500676a83638", null ],
    [ "callback", "Lab0x03__UI_8py.html#a8e61fdfd88105ce8b653cf6705f30751", null ],
    [ "dataCollect", "Lab0x03__UI_8py.html#a0a1b53710ad2fd7792854f4fb24872d0", null ],
    [ "dataSplit", "Lab0x03__UI_8py.html#a142b663021915f1ee29732474c9ac810", null ],
    [ "dataStrip", "Lab0x03__UI_8py.html#a30776e2297ff22d5c64c6013382d4b17", null ],
    [ "f", "Lab0x03__UI_8py.html#a1fe9270b27c5bf87fc8a77904b43b4fd", null ],
    [ "last_key", "Lab0x03__UI_8py.html#a76fbdff91c016b13064b5301944facdc", null ],
    [ "n", "Lab0x03__UI_8py.html#a950d8148b30cf985905522bcf8eafba2", null ],
    [ "ser", "Lab0x03__UI_8py.html#a2922d2d78192fd00f927605816c6d4af", null ],
    [ "times", "Lab0x03__UI_8py.html#a0b16c37314ed51b49b70beacd21f12cf", null ],
    [ "value", "Lab0x03__UI_8py.html#ad4b9050832e97987c4bbeda70e96c533", null ],
    [ "vdd", "Lab0x03__UI_8py.html#a09884a2cbb5e30e3442b1f7646fe615f", null ],
    [ "voltages", "Lab0x03__UI_8py.html#a82a27d8b157256438d35e8808fc11bd3", null ],
    [ "volts", "Lab0x03__UI_8py.html#a63f06e31ce776c06cc12ff44c1bb616a", null ]
];
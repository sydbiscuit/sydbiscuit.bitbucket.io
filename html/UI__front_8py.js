var UI__front_8py =
[
    [ "kb_cb", "UI__front_8py.html#aa21ab56e8f0029f944c615900213ebdd", null ],
    [ "write_csv", "UI__front_8py.html#a4845b4c7a9d25e1bd8de217b9f18e11c", null ],
    [ "axs", "UI__front_8py.html#ab09b7137f5c4ea30b97eb963b41c8e8c", null ],
    [ "callback", "UI__front_8py.html#a2a5065e85ce258db03bb6386ee210e98", null ],
    [ "dataCollect", "UI__front_8py.html#a1b1421f1a05d0b3f0bc1d7df2faa4818", null ],
    [ "dataSplit", "UI__front_8py.html#ac5e88387862ea3bebc66b61c111fe289", null ],
    [ "dataStrip", "UI__front_8py.html#a1e188bb1e7521c018e23b95c80b87f2d", null ],
    [ "f", "UI__front_8py.html#ac48180a44f2a768bd74f3bc1487db1cb", null ],
    [ "fig", "UI__front_8py.html#ab1512d814eec3359915113d46a4b0c74", null ],
    [ "last_key", "UI__front_8py.html#a9302a22ed2fe8675103abe8333282a9c", null ],
    [ "n", "UI__front_8py.html#abf21c887e455a882cff1abe34d4c962f", null ],
    [ "position", "UI__front_8py.html#a1da5a34756f1f11c40ea59e222ce2a1e", null ],
    [ "ser", "UI__front_8py.html#a26d65cb7ea7d47e295007598eedfb6ba", null ],
    [ "times", "UI__front_8py.html#af89462cd9109734465ca54dcd2e724cb", null ],
    [ "value", "UI__front_8py.html#ad2d4bbde3cf3e5e563a51491a8de0e08", null ],
    [ "values", "UI__front_8py.html#a3ce1c8f95b4e00ba47d3b94609f51db7", null ]
];
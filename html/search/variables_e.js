var searchData=
[
  ['sel_5faxis_466',['sel_axis',['../TouchDriver_8py.html#a4c53f9750f750bec4a91f9e6422a351b',1,'TouchDriver']]],
  ['sel_5ftest_467',['sel_test',['../TouchDriver_8py.html#ae2eac6d2611255cccc00181cda5c4a85',1,'TouchDriver']]],
  ['ser_468',['ser',['../Lab0x03__UI_8py.html#a2922d2d78192fd00f927605816c6d4af',1,'Lab0x03_UI.ser()'],['../UI__front_8py.html#a26d65cb7ea7d47e295007598eedfb6ba',1,'UI_front.ser()']]],
  ['ser_5fnum_469',['ser_num',['../classtask__share_1_1Queue.html#a6f9d87b116eb16dba0867d3746af9f5f',1,'task_share.Queue.ser_num()'],['../classtask__share_1_1Share.html#a2e8df029af46fbfd44ef0c2e7e8c7af6',1,'task_share.Share.ser_num()']]],
  ['share_5flist_470',['share_list',['../task__share_8py.html#a75818e5b662453e3723d0f234c85e519',1,'task_share']]],
  ['sign_471',['sign',['../classencoderDriver_1_1encoderDriver.html#a73732cf55c143c11c62d41067c66cffd',1,'encoderDriver::encoderDriver']]],
  ['speed1meas_472',['speed1Meas',['../shares_8py.html#ad222a2ab5d42683f9f3f345690f79884',1,'shares']]],
  ['speed2meas_473',['speed2Meas',['../shares_8py.html#ac4f6f801c42c3262b6d70b47ecc5af95',1,'shares']]],
  ['speedmeas_474',['speedMeas',['../classencoderDriver_1_1encoderDriver.html#a7a287aa01866376d90240b59712cc58e',1,'encoderDriver::encoderDriver']]],
  ['speedref_475',['speedRef',['../shares_8py.html#ae53517fcf4463ec229ed9638d71c6b3d',1,'shares']]],
  ['start_476',['start',['../Lab0x02A_8py.html#a63ae0b6af1a104a34dc5a4b9d325d9c6',1,'Lab0x02A.start()'],['../TouchDriver_8py.html#a8b05ca676f0a3deb2549c1eed9f0a789',1,'TouchDriver.start()']]],
  ['state_477',['state',['../bnoTEST_8py.html#ada5bd05fb065e086f0622e20f5657c53',1,'bnoTEST.state()'],['../Lab0x02A_8py.html#a5a500f087e617cb2dac0d0aadda59d6b',1,'Lab0x02A.state()'],['../Lab0x02B_8py.html#a92d17aef9aa97d34fae670615c4cdbe7',1,'Lab0x02B.state()'],['../Lab0x03__main_8py.html#a86ffcb8b30a9ab79f10e596d2fbdd08d',1,'Lab0x03_main.state()'],['../Lab0x04__main_8py.html#a025307ed3f9943bf2f57f2e073a66eda',1,'Lab0x04_main.state()']]],
  ['stop_478',['stop',['../TouchDriver_8py.html#afb89a36463df1e2b0a4dba08116d9c12',1,'TouchDriver']]]
];

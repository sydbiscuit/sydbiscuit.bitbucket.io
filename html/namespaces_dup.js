var namespaces_dup =
[
    [ "avg", null, [
      [ "avg", "avg_8py.html#a454e437b7c81f3bfd2506be52123099e", null ]
    ] ],
    [ "backEndTask", null, [
      [ "backEndTask", "backEndTask_8py.html#a2e19a44e66f3b265864673144f40f1d6", null ]
    ] ],
    [ "BNO055", null, [
      [ "bno055", "classBNO055_1_1bno055.html", "classBNO055_1_1bno055" ]
    ] ],
    [ "bnoTask", null, [
      [ "bnoTask", "bnoTask_8py.html#ae52f97151aa3f8c076c0fa2c7ee88d5e", null ]
    ] ],
    [ "bnoTEST", null, [
      [ "angle", "bnoTEST_8py.html#a10cebee6e057e8c43788c36037ac3f95", null ],
      [ "baudrate", "bnoTEST_8py.html#a962f5310323dc06796479c046c75ea32", null ],
      [ "dma", "bnoTEST_8py.html#afd2954e5f61ffec144579fdd97e3c73b", null ],
      [ "False", "bnoTEST_8py.html#a7c8a99f24e4b48216c0386dbca10dd8a", null ],
      [ "gencall", "bnoTEST_8py.html#a092c14782248cc9269b3b03406575355", null ],
      [ "i2c", "bnoTEST_8py.html#af46246f4ef87b03dfe87b2391a62bbb9", null ],
      [ "imu", "bnoTEST_8py.html#a8a93dda8448f8a24a2304d67076ca36e", null ],
      [ "MASTER", "bnoTEST_8py.html#ac75256ce0a5635674b2f42defd6523eb", null ],
      [ "omega", "bnoTEST_8py.html#adab8cee3bb424f442b903e86957bb19b", null ],
      [ "state", "bnoTEST_8py.html#ada5bd05fb065e086f0622e20f5657c53", null ]
    ] ],
    [ "closedLoop", null, [
      [ "closedLoop", "classclosedLoop_1_1closedLoop.html", "classclosedLoop_1_1closedLoop" ]
    ] ],
    [ "controllerTask", null, [
      [ "controllerTask", "controllerTask_8py.html#a1c50a8c9e91ae84b0393b82439a91568", null ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ],
      [ "task_list", "cotask_8py.html#ae54e25f8d14958f642bcc22ddeb6c56f", null ]
    ] ],
    [ "dataCollectTask", null, [
      [ "dataCollectTask", "dataCollectTask_8py.html#a49259461139a45ad35b98c5657277088", null ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847_channel", "classDRV8847_1_1DRV8847__channel.html", "classDRV8847_1_1DRV8847__channel" ]
    ] ],
    [ "encoderDriver", null, [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ],
      [ "delta", "encoderDriver_8py.html#a42f97ebcf0428bbc28aa6daf476c04cd", null ],
      [ "e1", "encoderDriver_8py.html#ad445cc88a3ae72c1c3de2ea52d1f7c04", null ],
      [ "e2", "encoderDriver_8py.html#aba82a1dc863e4ee8d712d9b316bd270d", null ],
      [ "enc", "encoderDriver_8py.html#aa84ea4ad1bb876b68b45b6a7fa39c810", null ],
      [ "ENC_AB", "encoderDriver_8py.html#a10cb8a0993137a61c2ff66419fdb56f1", null ],
      [ "interval", "encoderDriver_8py.html#a6be7ddf0eb592cfd46b5badc98199062", null ],
      [ "PB6", "encoderDriver_8py.html#ab36314f3fd403d7797e3e1d22834fa52", null ],
      [ "PB7", "encoderDriver_8py.html#a7ea6f224823bb0b52a3c054acc3c4688", null ],
      [ "PC6", "encoderDriver_8py.html#a643a379101214e9d3e870a658ed79dc9", null ],
      [ "PC7", "encoderDriver_8py.html#a2f424e81992d00df7d11ae73e1c112f3", null ],
      [ "pin", "encoderDriver_8py.html#acdd65e1dd64bf3b1f52917ea047e7b08", null ],
      [ "position", "encoderDriver_8py.html#aaecd066281336bbd56ba1bd764fd995f", null ],
      [ "start", "encoderDriver_8py.html#a4999770b7eed8a0c869cb29f1e5c346d", null ],
      [ "stop", "encoderDriver_8py.html#a3e35e9fd6687fd366df0e3d347a2e578", null ],
      [ "test", "encoderDriver_8py.html#ade48986a3898b0e2474352829e5429ba", null ],
      [ "TIM4", "encoderDriver_8py.html#a846859fef59320e765aa982a170c04d3", null ],
      [ "TIM8", "encoderDriver_8py.html#a997fb79056375b89c5597242cc0f1e20", null ]
    ] ],
    [ "encoderTask", null, [
      [ "encoderTask", "encoderTask_8py.html#a7764d95ac11cd3c6ca3a7a8f9bed5f62", null ]
    ] ],
    [ "encoderTEST", null, [
      [ "encoderTEST", "encoderTEST_8py.html#a23a0e00f20d12d7a85c67758cecb7248", null ]
    ] ],
    [ "fir", null, [
      [ "fir", "fir_8py.html#a62c33cee4553f328f4a9ba38ccda2e29", null ]
    ] ],
    [ "Lab0x01", "namespaceLab0x01.html", [
      [ "getChange", "namespaceLab0x01.html#a9095012be334c6cedff703d505fe7755", null ],
      [ "kb_cb", "namespaceLab0x01.html#aa0adb496187dcc6a358bfe2d9a77c91a", null ],
      [ "VendotronTask", "namespaceLab0x01.html#a8044965949c1b8712b2f43e31c26f99a", null ],
      [ "callback", "namespaceLab0x01.html#a630f74018672d5e111c4480071eeb816", null ],
      [ "coinDict", "namespaceLab0x01.html#ac7a97ee2f5a443a6447755c4fafbe0d3", null ],
      [ "denoms", "namespaceLab0x01.html#ade97675b645200bf217bc9d494aa1698", null ],
      [ "drinkDict", "namespaceLab0x01.html#a25a40fd8b6778dbe0c30798f56bbe5e5", null ],
      [ "Eject", "namespaceLab0x01.html#a96369c58d47c053cd8c167880d7bd999", null ],
      [ "last_key", "namespaceLab0x01.html#a4edfc48a94ba6f84500e1237350b6da6", null ],
      [ "vendo", "namespaceLab0x01.html#a696cdcee8c072ce9ca4e12f10cade58d", null ]
    ] ],
    [ "Lab0x02A", null, [
      [ "comp_average", "Lab0x02A_8py.html#ac4dd0572354724d68fe4367bf480999a", null ],
      [ "onButtonPressFCN", "Lab0x02A_8py.html#aa22a0f6b8e31fa7e40387f082d8bbfcb", null ],
      [ "ButtonInt", "Lab0x02A_8py.html#a59d769bce31aeeee3bdf64e9401c9ab3", null ],
      [ "current", "Lab0x02A_8py.html#abc083f49e33673b2a8f5c3c50c694227", null ],
      [ "elapsedTime", "Lab0x02A_8py.html#af877746ca664e1dc3ee23bdb36118a2d", null ],
      [ "myVar", "Lab0x02A_8py.html#aaa60418f61e5c1b1b3a620d6ae7f587d", null ],
      [ "pinC13", "Lab0x02A_8py.html#a7b7d814d02c9c69ab7b57c8f66d302ba", null ],
      [ "pinLED", "Lab0x02A_8py.html#a3fc550675f4de535f4149174aa0b90f1", null ],
      [ "reactionTime", "Lab0x02A_8py.html#a02a11ba5cd1c13a0872ff6082471281a", null ],
      [ "start", "Lab0x02A_8py.html#a63ae0b6af1a104a34dc5a4b9d325d9c6", null ],
      [ "state", "Lab0x02A_8py.html#a5a500f087e617cb2dac0d0aadda59d6b", null ],
      [ "t2ch1", "Lab0x02A_8py.html#aa7d734ba286098060671dd2721f4ac1d", null ],
      [ "tim2", "Lab0x02A_8py.html#aaa9680f58f7ad2491070745c08edfc7d", null ]
    ] ],
    [ "Lab0x02B", null, [
      [ "comp_average", "Lab0x02B_8py.html#a8ab24bc7e4cd91ad38b54ab6d2cef586", null ],
      [ "IC_Callback", "Lab0x02B_8py.html#a8ced9b0a8c41eacaab69f60ba429d2b0", null ],
      [ "OC_Callback", "Lab0x02B_8py.html#acd7f4c1dd49cac31c39ac262e4c2b457", null ],
      [ "_substate", "Lab0x02B_8py.html#a82127028d654b282e4b196739a081a1b", null ],
      [ "elapsedTime", "Lab0x02B_8py.html#aeca134fd62f5a85ea173b821f546e080", null ],
      [ "last_OC", "Lab0x02B_8py.html#ac1d88b044d61364fdf62e2279cbbb4ae", null ],
      [ "PB3", "Lab0x02B_8py.html#a8a3377ad78e3cca0d58d0ac58efa953d", null ],
      [ "pinC13", "Lab0x02B_8py.html#ad5a51a9de6c50982cfe699c032bb708c", null ],
      [ "pinLED", "Lab0x02B_8py.html#a32970c7b4b86c782bdfe521a2e9b7b61", null ],
      [ "reactionTime", "Lab0x02B_8py.html#a770f5695bec422d7690f57bd5b48a743", null ],
      [ "state", "Lab0x02B_8py.html#a92d17aef9aa97d34fae670615c4cdbe7", null ],
      [ "t2ch1", "Lab0x02B_8py.html#a050aef3f58d04d61cfe2bf7325facf56", null ],
      [ "t2ch2", "Lab0x02B_8py.html#ae730ca0018e3967fb00b49214006967c", null ],
      [ "tim2", "Lab0x02B_8py.html#a6b49bdc344fc301d4dbc35814e5f11d8", null ]
    ] ],
    [ "Lab0x03_main", null, [
      [ "onButtonPressFCN", "Lab0x03__main_8py.html#af52b85f6b6b3c2b8ac2cefcc516abf3d", null ],
      [ "acdc", "Lab0x03__main_8py.html#a0ad762cecf46eac46a71482d9b19f7f7", null ],
      [ "adc", "Lab0x03__main_8py.html#ae356c235ae3ea99f4d8f0cb4ef14cc5e", null ],
      [ "ADC", "Lab0x03__main_8py.html#a29582cb1395476521463fc38a17dd084", null ],
      [ "ButtonInt", "Lab0x03__main_8py.html#a770d346420bff5c3d8d583b71e198aa7", null ],
      [ "myuart", "Lab0x03__main_8py.html#a2a71bb3262503a833f6b028cd1f7de42", null ],
      [ "myVar", "Lab0x03__main_8py.html#a2c18a1f08c4592cc287a86e6be1e4487", null ],
      [ "n", "Lab0x03__main_8py.html#ad83e9799f4b357957598d315e6bd8780", null ],
      [ "period", "Lab0x03__main_8py.html#a31a5ebd5cc5dccfe67720873c601283e", null ],
      [ "pinC13", "Lab0x03__main_8py.html#a54a0389a379209889e84e8f215764637", null ],
      [ "sstate", "Lab0x03__main_8py.html#ac997bf3c7604db36e1d4e0ea1b1cdf22", null ],
      [ "state", "Lab0x03__main_8py.html#a86ffcb8b30a9ab79f10e596d2fbdd08d", null ],
      [ "tim2", "Lab0x03__main_8py.html#a5bb0d8eaa8922cb9e24d065e929d38f9", null ],
      [ "times", "Lab0x03__main_8py.html#aa159f8d6899496b2865c48a78d5218bf", null ],
      [ "val", "Lab0x03__main_8py.html#ab095960b82511eb10f2298bc912b0579", null ]
    ] ],
    [ "Lab0x03_UI", null, [
      [ "kb_cb", "Lab0x03__UI_8py.html#a779c6658c226efa5b1cbf36b7f185271", null ],
      [ "write_csv", "Lab0x03__UI_8py.html#abf903f6f7873220b29d8b40c9cc6f4db", null ],
      [ "ADC", "Lab0x03__UI_8py.html#a7cafd15c801a7e5c3ec9500676a83638", null ],
      [ "callback", "Lab0x03__UI_8py.html#a8e61fdfd88105ce8b653cf6705f30751", null ],
      [ "dataCollect", "Lab0x03__UI_8py.html#a0a1b53710ad2fd7792854f4fb24872d0", null ],
      [ "dataSplit", "Lab0x03__UI_8py.html#a142b663021915f1ee29732474c9ac810", null ],
      [ "dataStrip", "Lab0x03__UI_8py.html#a30776e2297ff22d5c64c6013382d4b17", null ],
      [ "f", "Lab0x03__UI_8py.html#a1fe9270b27c5bf87fc8a77904b43b4fd", null ],
      [ "last_key", "Lab0x03__UI_8py.html#a76fbdff91c016b13064b5301944facdc", null ],
      [ "n", "Lab0x03__UI_8py.html#a950d8148b30cf985905522bcf8eafba2", null ],
      [ "ser", "Lab0x03__UI_8py.html#a2922d2d78192fd00f927605816c6d4af", null ],
      [ "times", "Lab0x03__UI_8py.html#a0b16c37314ed51b49b70beacd21f12cf", null ],
      [ "value", "Lab0x03__UI_8py.html#ad4b9050832e97987c4bbeda70e96c533", null ],
      [ "vdd", "Lab0x03__UI_8py.html#a09884a2cbb5e30e3442b1f7646fe615f", null ],
      [ "voltages", "Lab0x03__UI_8py.html#a82a27d8b157256438d35e8808fc11bd3", null ],
      [ "volts", "Lab0x03__UI_8py.html#a63f06e31ce776c06cc12ff44c1bb616a", null ]
    ] ],
    [ "Lab0x04_main", null, [
      [ "MCP9808", "classLab0x04__main_1_1MCP9808.html", "classLab0x04__main_1_1MCP9808" ],
      [ "adcall", "Lab0x04__main_8py.html#a4c6c8cfa670b3c206592118643ec1e21", null ],
      [ "baudrate", "Lab0x04__main_8py.html#ac2071c335600ad79f66f780874443f1e", null ],
      [ "data", "Lab0x04__main_8py.html#a2cbc0186ac257dd8868482a644f94a41", null ],
      [ "data_l", "Lab0x04__main_8py.html#acc020235234f6fe97123d9ce4f09be9a", null ],
      [ "data_u", "Lab0x04__main_8py.html#a8725111f026bfa248acec37c9273ce99", null ],
      [ "dma", "Lab0x04__main_8py.html#a57deba694c0815bf71ac13e302c152ba", null ],
      [ "False", "Lab0x04__main_8py.html#a2eeb365a1e5706cf7e1131bd9e894b28", null ],
      [ "gencall", "Lab0x04__main_8py.html#aef2694fb69f7267c31c2684d49fc6ef9", null ],
      [ "i2c_1", "Lab0x04__main_8py.html#a9725858f69458bcda86a51ccc42e18fe", null ],
      [ "ID", "Lab0x04__main_8py.html#ade2ca7e516476e1a896be2e92fa0cbf5", null ],
      [ "MASTER", "Lab0x04__main_8py.html#a8f4cc5683ba7bc860cc07fd9f73c07da", null ],
      [ "mc_F", "Lab0x04__main_8py.html#a93173cbbc520cd71e1e5de16e01847e1", null ],
      [ "period", "Lab0x04__main_8py.html#a00fe864a46e7f0c30f5e259421f7d5b1", null ],
      [ "sensor", "Lab0x04__main_8py.html#afbe45c6378414f980bc525a913bbf452", null ],
      [ "state", "Lab0x04__main_8py.html#a025307ed3f9943bf2f57f2e073a66eda", null ],
      [ "temp_mc", "Lab0x04__main_8py.html#a28d1920dd4f223712264881b57f1ca13", null ],
      [ "temp_sens", "Lab0x04__main_8py.html#a893dfa93d7f8e0e0bf69e1202e3790e8", null ],
      [ "Tf", "Lab0x04__main_8py.html#a918afee2bc4bf54231239b5036deabd6", null ],
      [ "time", "Lab0x04__main_8py.html#a2d4ffeb0bc6d19b27b40886fe1afac2a", null ],
      [ "val", "Lab0x04__main_8py.html#ad338c0f8c2644498e49a40dc841e7ba2", null ]
    ] ],
    [ "Lab0xFF_main", null, [
      [ "conTask", "Lab0xFF__main_8py.html#a386cac9f15a418a9782aa3b5e5e78738", null ],
      [ "datTask", "Lab0xFF__main_8py.html#ad44785cfadfca5c8a509d21a0bc70700", null ],
      [ "encTask", "Lab0xFF__main_8py.html#aa99f8559820daa34ca5ef2d877c63070", null ],
      [ "endTask", "Lab0xFF__main_8py.html#a4ce1a488310dec825a1c7e1eba21a78c", null ],
      [ "motTask", "Lab0xFF__main_8py.html#a6177b00133e6f3fb8f8b2d2c29082b07", null ],
      [ "priTask", "Lab0xFF__main_8py.html#a5c1dfa80bd36ba054f7ed73478be5363", null ],
      [ "RTPTask", "Lab0xFF__main_8py.html#a5b5cf4f67ff04d2e7d59189d606dea95", null ]
    ] ],
    [ "main", null, [
      [ "conTask", "main_8py.html#a988d215ac143ae944c1dd84ba8f5c336", null ],
      [ "datTask", "main_8py.html#aa457c7860e20465f25219be7c57e935d", null ],
      [ "encTask", "main_8py.html#aea8952b5afc8c1b84c8db14cddd3e249", null ],
      [ "endTask", "main_8py.html#ad159dc73f6dee8b5be13708b56dc7e74", null ],
      [ "motTask", "main_8py.html#afd61d5b7c9bd88a44bd7b2bc291e42f1", null ],
      [ "priTask", "main_8py.html#a95148a65453a330c6dd8f559a23a039f", null ],
      [ "RTPTask", "main_8py.html#a688cdc5bb2a1dcac3ae6278544c9a318", null ]
    ] ],
    [ "ME405_HW01_LEWIS_040621", null, [
      [ "getChange", "ME405__HW01__LEWIS__040621_8py.html#a59cb9ff5802bdba325b4168a726f5976", null ],
      [ "cents", "ME405__HW01__LEWIS__040621_8py.html#abe066e40897e899711badb06a6f224c7", null ],
      [ "change", "ME405__HW01__LEWIS__040621_8py.html#adf172aac314b9b1abb96399e3e279108", null ],
      [ "number", "ME405__HW01__LEWIS__040621_8py.html#a3397967131f147f9fc19c7268556c9c7", null ],
      [ "payment", "ME405__HW01__LEWIS__040621_8py.html#a2c05a201e171b97d617e0f541a4c3dac", null ],
      [ "price", "ME405__HW01__LEWIS__040621_8py.html#ad7e443943becacb3b7ed5f6c4a020498", null ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorTask", null, [
      [ "MotorTask", "MotorTask_8py.html#a15bc5e702aece1a3292d56e5bed235e0", null ]
    ] ],
    [ "print_task", null, [
      [ "put", "print__task_8py.html#a2986427f884f4edfc5d212b2f99f1f23", null ],
      [ "put_bytes", "print__task_8py.html#a6172f74f0655d6d9288284aab62dd7fe", null ],
      [ "run", "print__task_8py.html#abe2a60b9d48d38a4c9ec85bd891aafca", null ],
      [ "BUF_SIZE", "print__task_8py.html#a88f04f8dbccfe4f94fc04afbc194a7b0", null ],
      [ "print_queue", "print__task_8py.html#a81414bedb3face3c011fdde4579a04f7", null ],
      [ "print_task", "print__task_8py.html#aeb44d382e1d09e84db0909b53b9b1d13", null ],
      [ "PROFILE", "print__task_8py.html#a959384ca303efcf0bcfd7f12469d1f09", null ],
      [ "THREAD_PROTECT", "print__task_8py.html#a11e4727a312bb3d5da524affe5fc462f", null ]
    ] ],
    [ "refund", null, [
      [ "getChange", "refund_8py.html#ab857b88805b4ec39d2b910abc21923bf", null ]
    ] ],
    [ "shares", null, [
      [ "alpha_gain", "shares_8py.html#af74423c00aac51e9048b062aac662ce9", null ],
      [ "BACKstate", "shares_8py.html#aec4153ce0a80e176d50349d9ec2ee4ee", null ],
      [ "beta_gain", "shares_8py.html#a309d0ea046d5bbb9c64532220a3463c3", null ],
      [ "encoder2Position", "shares_8py.html#ac1e0b39e80518644cc8127b66ac42e20", null ],
      [ "encoderPosition", "shares_8py.html#adee4a6ca231638a930a1cc841b3178a9", null ],
      [ "ENCODERstate", "shares_8py.html#abbf96cfe710c8d8331b50e6e9f9c07be", null ],
      [ "pos1Ref", "shares_8py.html#ab7cccdff76430d7da8ae41434c587c87", null ],
      [ "pos2Ref", "shares_8py.html#a1627d4a08ff5c52f2dde2ff6970f356c", null ],
      [ "posMeas", "shares_8py.html#aaac4cef55a02d9972651a7bdf71720ad", null ],
      [ "PWM1", "shares_8py.html#a17ae7220cac4183b25d46b57b80a077b", null ],
      [ "PWM2", "shares_8py.html#a781500ea75dc4796c30c49e8465f4447", null ],
      [ "speed1Meas", "shares_8py.html#ad222a2ab5d42683f9f3f345690f79884", null ],
      [ "speed2Meas", "shares_8py.html#ac4f6f801c42c3262b6d70b47ecc5af95", null ],
      [ "speedRef", "shares_8py.html#ae53517fcf4463ec229ed9638d71c6b3d", null ],
      [ "tref1", "shares_8py.html#a28e9eafe3dacfb649abe881834a5a60f", null ],
      [ "tref2", "shares_8py.html#a37bc2e506c28a8782223f93adc7161dd", null ],
      [ "vx", "shares_8py.html#ad98765c924f3cfeeb1676f85428d8d6b", null ],
      [ "vy", "shares_8py.html#aff76c441088c6a30e47c82d5f72f1503", null ],
      [ "x", "shares_8py.html#aa3104f8fdb6091da6ec17acaa5fe0087", null ],
      [ "y", "shares_8py.html#a29c058d83fed912720d2cc7e93ed8b43", null ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ],
      [ "show_all", "task__share_8py.html#a130cad0bc96d3138e77344ea85586b7c", null ],
      [ "share_list", "task__share_8py.html#a75818e5b662453e3723d0f234c85e519", null ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ],
      [ "PIN_xm", "TouchDriver_8py.html#a0aafc01a19eec0baab5a56e8e5c34896", null ],
      [ "PIN_xp", "TouchDriver_8py.html#a4e1a353719dada5e6bb13e6cb481f176", null ],
      [ "PIN_ym", "TouchDriver_8py.html#a2378adb5307f7346aa5e8f1ab33982bc", null ],
      [ "PIN_yp", "TouchDriver_8py.html#add184b246838e14a526692fa1b3495ec", null ],
      [ "pos", "TouchDriver_8py.html#ac4b08bf5c1c03afce2e884df50acd64b", null ],
      [ "run_time", "TouchDriver_8py.html#a36eb81da32fa799b3594d29f7ee06938", null ],
      [ "sel_axis", "TouchDriver_8py.html#a4c53f9750f750bec4a91f9e6422a351b", null ],
      [ "sel_test", "TouchDriver_8py.html#ae2eac6d2611255cccc00181cda5c4a85", null ],
      [ "start", "TouchDriver_8py.html#a8b05ca676f0a3deb2549c1eed9f0a789", null ],
      [ "stop", "TouchDriver_8py.html#afb89a36463df1e2b0a4dba08116d9c12", null ],
      [ "TouchPanel", "TouchDriver_8py.html#ad105ffbd9d3de069b2dbf1c087abda51", null ],
      [ "x", "TouchDriver_8py.html#a0b46735b2a08a636bb5378142f302c98", null ],
      [ "y", "TouchDriver_8py.html#acdacbd83d4bdf237e411f3a6388fbaaf", null ],
      [ "z", "TouchDriver_8py.html#a4f219e389d4ece1d46264e5e7d44f4c1", null ]
    ] ],
    [ "TouchPanelTask", null, [
      [ "TouchPanelTask", "TouchPanelTask_8py.html#a9338e069225fc3f6c1f560540d1aba54", null ]
    ] ],
    [ "UI_front", null, [
      [ "kb_cb", "UI__front_8py.html#aa21ab56e8f0029f944c615900213ebdd", null ],
      [ "write_csv", "UI__front_8py.html#a4845b4c7a9d25e1bd8de217b9f18e11c", null ],
      [ "axs", "UI__front_8py.html#ab09b7137f5c4ea30b97eb963b41c8e8c", null ],
      [ "callback", "UI__front_8py.html#a2a5065e85ce258db03bb6386ee210e98", null ],
      [ "dataCollect", "UI__front_8py.html#a1b1421f1a05d0b3f0bc1d7df2faa4818", null ],
      [ "dataSplit", "UI__front_8py.html#ac5e88387862ea3bebc66b61c111fe289", null ],
      [ "dataStrip", "UI__front_8py.html#a1e188bb1e7521c018e23b95c80b87f2d", null ],
      [ "f", "UI__front_8py.html#ac48180a44f2a768bd74f3bc1487db1cb", null ],
      [ "fig", "UI__front_8py.html#ab1512d814eec3359915113d46a4b0c74", null ],
      [ "last_key", "UI__front_8py.html#a9302a22ed2fe8675103abe8333282a9c", null ],
      [ "n", "UI__front_8py.html#abf21c887e455a882cff1abe34d4c962f", null ],
      [ "position", "UI__front_8py.html#a1da5a34756f1f11c40ea59e222ce2a1e", null ],
      [ "ser", "UI__front_8py.html#a26d65cb7ea7d47e295007598eedfb6ba", null ],
      [ "times", "UI__front_8py.html#af89462cd9109734465ca54dcd2e724cb", null ],
      [ "value", "UI__front_8py.html#ad2d4bbde3cf3e5e563a51491a8de0e08", null ],
      [ "values", "UI__front_8py.html#a3ce1c8f95b4e00ba47d3b94609f51db7", null ]
    ] ],
    [ "vendorLab0x01", null, [
      [ "getChange", "vendorLab0x01_8py.html#a6c0a0e79f55a812de856c3b960e60986", null ],
      [ "kb_cb", "vendorLab0x01_8py.html#a97400875364c0c566a3ed10c12096485", null ],
      [ "VendotronTask", "vendorLab0x01_8py.html#abb52fe2717a0469a4b834e64f60260b1", null ],
      [ "callback", "vendorLab0x01_8py.html#a6a58d8ec1ec328b579fb688da5079339", null ],
      [ "drinkDict", "vendorLab0x01_8py.html#a9f539c8bf4f5f90061c9d6b5dac365fc", null ],
      [ "last_key", "vendorLab0x01_8py.html#a442dc8b3080000f269d79216011956cf", null ],
      [ "moneyDict", "vendorLab0x01_8py.html#a9a9dc5b26c124016b1bb2f6084528151", null ],
      [ "vendo", "vendorLab0x01_8py.html#a458238a61b98fef377714792e1067b7e", null ]
    ] ]
];
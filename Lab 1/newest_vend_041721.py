"""@file            Lab0x01.py
   @brief           This file contains a FSM example using a vending machine.
   @author          Sydney Lewis
   @date            04/14/21
   
   @package         Lab0x01
   @brief           This package contains the entire lab script for Lab 1.
   @author          Sydney Lewis
   @date            April 14, 2021
"""

# Author: Sydney Lewis
# Date: 04/15/21

# Main script for FSM

drinkDict = {'c':('Cuke',100), 'p':('Popsi',120), 's':('Spryte',85),
             'd':('Dr. Pupper',110), '0':('Nothing',0)}
coinDict = {'0':('Penny',1), '1':('Nickle',5), '2':('Dime',10), '3':('Quarter',25), 
            '4':('One dollar',100), '5':('Five dollars',500), '6':('Ten dollars',1000), 
            '7':('Twenty dollars',2000)}
Eject = {'e':('Eject',0)}
denoms = [['Penny',       1],
          ['Nickle',      5],
          ['Dime',       10],
          ['Quarter',    25],
          ['$1 Bill',   100],
          ['$5 Bill',   500],
          ['$10 Bill', 1000],
          ['$20 Bill', 2000]]

#coinDict = {'0':1, '1':5, '2':10, '3':25, '4':100, '5':500, '6':1000, '7':2000}



## Keyboard loop for user input

import keyboard 

last_key = ''
    

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
 
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("q", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)

def getChange(price, pmt):
# '''
# @brief          Computes change for a monetary transaction.
# @details        Given the price of an item and the denominations to pay
#                 with, computes the proper change using the fewest number of
#                 denominations.
# @param price    The price of the item to purchase as an integer number of
#                 cents.
# @param pmt      A list containing the quantity for each denomination.
#                 Denominations are ordered smallest to largest: pennies, 
#                 nickels, dimes, quarters, ones, fives, tens, and twenties.
# @return         If the payment is sufficient, returns a  list representing
#                 the computed change in the same format as pmt. If payment
#                 is insufficient, returns None.
# '''
    

# Compute otal value of provided denominations
    pmtVal = sum(denomQty*denom[1] for denomQty, denom in zip(pmt,denoms))
    
    # Return early if there are insufficient funds
    if pmtVal < price:
        return None
    
    # If funds are sufficient, compute required change
    cngVal = pmtVal - price
    
    # Start an empty list to fill with denomination quantities
    cng = []
    
    # Starting with the biggest denomination, compute quantity of each.
    for denom in reversed(denoms):
        cng.insert(0, cngVal // denom[1])
        cngVal -= denom[1]*cng[0]
        
    return cng

# Run this loop forever, or at least until someone presses 'q'
# Code similar to this will go into specific states of your vendotron FSM

from time import sleep
 
def VendotronTask():
    """ Runs one iteration of the task
    """
    global last_key 
    state = 0
    bev_sel = '0'
    counter = 0
    balance = 0
    counter2 = 0
    pmt = [0,0,0,0,0,0,0,0]
    
    while True:
        # State 0: Initialization Message
        
        if state == 0:
            # perform state 0 operations
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            
            print("Select beverage at any time by pressing 'c', 'p', 's' or 'd'.")
            print("Return change by pressing 'e'.")
            

            state = 1       # on the next iteration, the FSM will run state 1
          
        # State 1: Checking for button presses   
        if state == 1:
            if counter == 310:
                print("Try Cuke Today!")
                counter = 0
            elif last_key in coinDict.keys():
                pay_sel = last_key
                last_key = ''
                counter = 0
                state = 2
            elif last_key in drinkDict.keys():
                bev_sel = last_key
                last_key = ''
                counter = 0
                state = 3
            elif last_key == 'e':
                eject = last_key
                last_key = ''
                state = 4
                counter = 0
            elif last_key == 'q':
                last_key = ''
                print("'q' detected. Quitting...")
                keyboard.unhook_all ()
                state = 0
            elif balance != 0:
                counter += 1
                state = 2
            else:
                counter += 1
                state = 1
                
        
        # State 2: Updating and printing balance; counting for 2nd prompt  
        elif state == 2:
            if counter2 == 250:
                print('Balance of\t${:.2f} remaining. Please select another beverage. Or "e" for change'.format(balance/100))
                counter2 = 0
                state = 1
            elif pay_sel == 'z':
                counter2 += 1
                state = 1
            else:
                state = 1      # on the next iteration, the FSM will go back to state 1
                counter2 = 0
                balance += coinDict[pay_sel][1]
                print('You have entered:', coinDict[pay_sel][0])
                print('Current Balance:\t${:.2f}'.format(balance/100))
                if pay_sel == '0':
                    pmt[0] = int(pmt[0]) + 1
                    pay_sel = 'z'
                elif pay_sel == '1':
                    pmt[1] = int(pmt[1]) + 1
                    pay_sel = 'z'
                elif pay_sel == '2':
                    pmt[2] = int(pmt[2]) + 1
                    pay_sel = 'z'
                elif pay_sel == '3':
                    pmt[3] = int(pmt[3]) + 1
                    pay_sel = 'z'
                elif pay_sel == '4':
                    pmt[4] = int(pmt[4]) + 1
                    pay_sel = 'z'
                elif pay_sel == '5':
                    pmt[5] = int(pmt[5]) + 1
                    pay_sel = 'z'
                elif pay_sel == '6':
                    pmt[6] = int(pmt[6]) + 1
                    pay_sel = 'z'
                elif pay_sel == '7':
                    pmt[7] = int(pmt[7]) + 1
                    pay_sel = 'z'
                elif pay_sel == 'e':
                    state = 3
                    
                
        
        # State 3: Beverage selection and balance update      
        elif state == 3:
            price = drinkDict[bev_sel][1]
            print('You have selected:', drinkDict[bev_sel][0])
            print('Price:\t${:.2f}'.format(price/100))
            state = 1
            if balance == 0:
                print('Insufficient Funds')
                print('Current Balance:\t${:.2f}'.format(balance/100))
            else:
                bev_sel = '0' 
                counter2 = 0
                change = getChange(price, pmt)
                if change == None:
                    print("Insufficient Funds - Add money or select a different beverage.")
                    print('Current Balance:\t${:.2f}'.format(balance/100))
                    state = 1
                else:
                    balance -= price
                    print('New balance:\t${:.2f}'.format(balance/100))
                    if pay_sel == 'e':
                        state = 4
                    else:
                        state = 1
                
                
            
                
             
                
         # perform state 4 operations: Eject change   
        elif state == 4:
            if balance == 0:
                print("No change to eject.")
                state = 0
            elif change != 0:
                print('Ejecting change:\t${:.2f}'.format(balance/100))
                print(change)
                balance = 0
                pmt = [0,0,0,0,0,0,0,0]
                state = 0   # s4 -> s1
            else:
                print('Ejecting change:\t${:.2f}'.format(balance/100))
                print(pmt)
                pmt = [0,0,0,0,0,0,0,0]
                balance = 0
                state = 0   # s4 -> s1
            
        else:
            # this state shouldn't exist!
            
            pass
        
        yield(state)


if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try: 
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')


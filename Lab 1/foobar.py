'''@file                    foobar.py
   @brief                   This file makes a string called bar
   @author                  Sydney Lewis
   @copyright               sydbiscuit 
   @date                    April 12, 2021
   
   @package                 foobar
   @brief                   foo-BAR baby
   @author                  Sydney Lewis
   @copyright               sydbiscuit 
   @date                    April 12, 2021
'''

## my string foobar

my_string = input('Please enter a string: ')
if my_string == 'foo':
    print('bar')
else:
    pass



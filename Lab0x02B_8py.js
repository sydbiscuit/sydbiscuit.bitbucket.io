var Lab0x02B_8py =
[
    [ "comp_average", "Lab0x02B_8py.html#a8ab24bc7e4cd91ad38b54ab6d2cef586", null ],
    [ "IC_Callback", "Lab0x02B_8py.html#a8ced9b0a8c41eacaab69f60ba429d2b0", null ],
    [ "OC_Callback", "Lab0x02B_8py.html#acd7f4c1dd49cac31c39ac262e4c2b457", null ],
    [ "_substate", "Lab0x02B_8py.html#a82127028d654b282e4b196739a081a1b", null ],
    [ "elapsedTime", "Lab0x02B_8py.html#aeca134fd62f5a85ea173b821f546e080", null ],
    [ "last_OC", "Lab0x02B_8py.html#ac1d88b044d61364fdf62e2279cbbb4ae", null ],
    [ "PB3", "Lab0x02B_8py.html#a8a3377ad78e3cca0d58d0ac58efa953d", null ],
    [ "pinC13", "Lab0x02B_8py.html#ad5a51a9de6c50982cfe699c032bb708c", null ],
    [ "pinLED", "Lab0x02B_8py.html#a32970c7b4b86c782bdfe521a2e9b7b61", null ],
    [ "reactionTime", "Lab0x02B_8py.html#a770f5695bec422d7690f57bd5b48a743", null ],
    [ "state", "Lab0x02B_8py.html#a92d17aef9aa97d34fae670615c4cdbe7", null ],
    [ "t2ch1", "Lab0x02B_8py.html#a050aef3f58d04d61cfe2bf7325facf56", null ],
    [ "t2ch2", "Lab0x02B_8py.html#ae730ca0018e3967fb00b49214006967c", null ],
    [ "tim2", "Lab0x02B_8py.html#a6b49bdc344fc301d4dbc35814e5f11d8", null ]
];
''' @file       encoderTEST.py
    @brief      Tests the encoder driver to ensure proper operation.

    @author     David Hernandez
    @author     Sydney Lewis
    @date       5/26/21
    @copyright  2020-2021 Hernandez & Lewis Inc.
'''

def encoderTEST():
    '''
    Test encoderDriver only within this file.
    '''
    
    import utime
    import pyb
    from pyb import Pin, Timer
    import encoderDriver
    from encoderDriver import encoderDriver
    import shares 
    
    encoderstate = 0
    ## Pin initialization for Encoders
    PB6 = Pin(pyb.Pin.cpu.B6)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinB6 for encoder
    #
    PB7 = Pin(pyb.Pin.cpu.B7)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinB7 for encoder
    #
    PC6 = Pin(pyb.Pin.cpu.C6)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinC6 for encoder
    #
    PC7 = Pin(pyb.Pin.cpu.C7)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinC7 for encoder
    #
    ## Encoder 1 timer set-up
    TIM4 = Timer(4, period=0xFFFF, prescaler=0)
    TIM4.channel(1, Timer.ENC_AB, pin = PB6)
    TIM4.channel(2, Timer.ENC_AB, pin = PB7)
    
    ## Encoder 2 timer set-up when required
    TIM8 = Timer(8, period=0xFFFF, prescaler=0)
    TIM8.channel(1, Timer.ENC_AB, pin = PC6)
    TIM8.channel(2, Timer.ENC_AB, pin = PC7)
    
    ## Encoder set up for Encoder 1 and Encoder 2
    e1 = encoderDriver(PB6, PB7, 4)
    ## @brief       Sets the first encoder up with pins B6 and B7 with timer 4
    # @details      Calls on encoderDriver class to input the required arguments into constructor.
     # @param       self.PB6 calls on pin C6 to get into first pin channel
     # @param       self.PB7 calls on pin C7 to get into second pin channel
     # @param       4 is the timer being used for the pin and channel inputs
    #
    e2 = encoderDriver(PC6, PC7, 8)
    ## @brief       Sets the second encoder up with pins C6 and C7 with timer 8
     # @details     Calls on encoderDriver class to input the required arguments into constructor.
     # @param       self.PC6 calls on pin C6 to get into first pin channel
     # @param       self.PC7 calls on pin C7 to get into second pin channel
     # @param       8 is the timer being used for the pin and channel inputs

    while True:
        e1.update()
        e2.update()
        position = e2.get_position()
        delta = e2.get_delta()
        speed1 = e1.get_speed()
        speed2 = e2.get_speed()
        print('Encoder 2 position: {:} [rad] delta: {:} [rad]'.format(position, delta))
        print('Encoder 2 speed: {:} [rad/s]'.format(speed2))
        e2.update()
        utime.sleep_ms(250)
        position = e1.get_position()
        delta = e1.get_delta()
        print('Encoder 1 position: {:} [rad] delta: {:} [rad]'.format(position, delta))
        print('Encoder 1 speed: {:} [rad/s]'.format(speed1))
        e1.update()
        utime.sleep_ms(250)
        yield(encoderstate)


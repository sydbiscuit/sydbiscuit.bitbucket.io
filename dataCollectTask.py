''' @file       dataCollectTask.py
    @brief      This file gathers data request by user via UI.
    @details    Sends formatted data collected from the encoder and motor
                between the Nucleo and computer.
                
                See source code here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/dataCollectTask.py
                    
    @author     David Hernandez
    @author     Sydney Lewis
    @date       6/8/21
    @copyright  2020-2021 Hernandez and Lewis Inc.
'''



import shares
import print_task


def dataCollectTask():
    ''' @brief      A task class that writes gathered motor speed and positional data to UI for processing.
        @details    When user interface commands are pressed, this task will work with other task
                    to gather any data user demands. Then using UART will write the data over to
                    the front end for processing.
    '''

    # State constants
    S0_INIT                 = 0 # User PRESSES:
    # S1_ZERO_POSITION        = 1 # Z
    # S2_PRINT_ENC_POSITION   = 2 # P
    # S3_PRINT_ENC_SPEED      = 3 # D
    S4_COLLECT_DATA         = 4 # G
    S5_STOP_COLLECTING      = 5 # S
    # S6_GET_ALPHA            = 6 # A
    # S7_GET_BETA             = 7 # B
    # S8_SET_ALPHA            = 8 # U
    # S9_SET_BETA             = 9 # I
    # S10_CLR_ERROR           = 10 # E
    # S11_BAL_BOARD           = 11 # F

    period = 1
    ## @brief   1 microsecond of elapsed time.
    #
    time = 0
    ## @brief   Total time elapsed.
    #
    # values = array('f',[0])
    # ##@brief Creates blank array for encoder 1 speed [deg/s]
    # #
    # position = array('f',[0])
    # ##@brief Creates blank array for encoder 1 position [deg]
    # #
    # values2 = array('f',[0])
    # ##@brief Creates blank array for encoder 2 speed [deg/s]
    # #
    # position2 = array('f',[0])
    # ##@brief Creates blank array for encoder 2 position [deg]
    # #
    # RTPvx = array('f',[0])
    # ##@brief Creates blank array for RTP x speed [m/s]
    # #
    # RTPx = array('f',[0])
    # ##@brief Creates blank array for RTP x position [m]
    # #
    # RTPvy = array('f',[0])
    # ##@brief Creates blank array for RTP y speed [m/s]
    # #
    # RTPy = array('f',[0])
    # ##@brief Creates blank array for RTP y position [m]
    # #

    # print_task.put('Data collection ready')
    print('DATA good')
    
    while True:
        
            e1Pos = shares.encoderPosition.get()
            e1Vel = shares.speed1Meas.get()
            e2Pos = shares.encoder2Position.get()
            e2Vel = shares.speed2Meas.get()
            
            RTPx  = shares.x.get()
            RTPvx = shares.vx.get()
            RTPy  = shares.y.get()
            RTPvy = shares.vy.get()
            ## Print data as CSV in console
            with open("Lab0xFF.csv", "a") as a_file:
                a_file.write("{:},{:},{:},{:},{:},{:},{:},{:},{:}\r\n".format(e1Pos,e1Vel,e2Pos,e2Vel, RTPvx, RTPx, RTPvy, RTPy, time))
            print_task.put('Data Written to CSV')
            time += period
            yield(0)
            ############ ------ FSM ------###########
            if shares.BACKstate == S0_INIT:
                pass

            ## If G button is pressed on keyboard it will transition to state 1
            elif shares.BACKstate == S4_COLLECT_DATA:

                pass
        
                # Standby for next iteration
                shares.BACKstate = S0_INIT
                pass
        
            else: 
                pass 
            yield(0)

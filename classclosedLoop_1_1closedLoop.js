var classclosedLoop_1_1closedLoop =
[
    [ "__init__", "classclosedLoop_1_1closedLoop.html#a9a83542a8cd0cfacdda6efd2f20f3674", null ],
    [ "get_alpha", "classclosedLoop_1_1closedLoop.html#aa862f6ddb145edb2d3c71c85d06c3df0", null ],
    [ "get_beta", "classclosedLoop_1_1closedLoop.html#aeb98c3049c9fe47b26c274aac1b201b5", null ],
    [ "get_delta", "classclosedLoop_1_1closedLoop.html#a5defbfb8944f44ef10fc01475295eb4b", null ],
    [ "get_pos", "classclosedLoop_1_1closedLoop.html#aeffcaf3f9c9ad476853acb6f54a79b22", null ],
    [ "RTP_filter", "classclosedLoop_1_1closedLoop.html#aeef986af8add44f9f3b91b0ff707d838", null ],
    [ "set_alpha", "classclosedLoop_1_1closedLoop.html#a996fa25115a2776ac8deebef4e9b034a", null ],
    [ "set_beta", "classclosedLoop_1_1closedLoop.html#a4b78941a8c587d4e2227033ed3badfdc", null ],
    [ "update", "classclosedLoop_1_1closedLoop.html#a4fb06ac940cec662209662f7f285f152", null ],
    [ "alpha_gain", "classclosedLoop_1_1closedLoop.html#ac68a370feb704ad52662aa438ceb7c4d", null ],
    [ "beta_gain", "classclosedLoop_1_1closedLoop.html#a31dfd3d2d4b2798f2513095effd135d1", null ],
    [ "K_gains", "classclosedLoop_1_1closedLoop.html#a8a54754affc01307125d431387143ccc", null ],
    [ "Kt", "classclosedLoop_1_1closedLoop.html#a4aa9d5f032fb09d6abddf50c05f7fca8", null ],
    [ "P_new", "classclosedLoop_1_1closedLoop.html#a94d99cab90c6ddf3ef911bc4499dd1de", null ],
    [ "Pmax", "classclosedLoop_1_1closedLoop.html#a1d8db0ec2baadc9fcefb3fea21d1f789", null ],
    [ "Pmin", "classclosedLoop_1_1closedLoop.html#afc9476fe9d986b9dca862c77c3d03563", null ],
    [ "PWM", "classclosedLoop_1_1closedLoop.html#afe6ed01366dfa44b8e1137cd93f54288", null ],
    [ "R", "classclosedLoop_1_1closedLoop.html#aeeb499d00747e1fdfc142e7f8d3ff156", null ],
    [ "V_k", "classclosedLoop_1_1closedLoop.html#a0e559b6e5fcf29c84d55d43e645b3c16", null ],
    [ "Vdc", "classclosedLoop_1_1closedLoop.html#a3c5fdec909374b03f8ef5f80aef2070a", null ],
    [ "X_k", "classclosedLoop_1_1closedLoop.html#a0edff7cf0b876c1d278672ba47eb4124", null ]
];
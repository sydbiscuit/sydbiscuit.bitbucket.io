# Lab 2 Part A 
import random
import utime
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)
state = 0
timer = 100

while True:
 
    if state == 0:
        blinko.low()
        light = random.randint(200, 300)
        if light > 0:
            light -= 1
        elif light == 0:
            state = 1
            
        if state == 1:
            start_time = utime.ticks_us ()
            blinko.high()
            if timer > 0:
                timer -= 1
            elif timer == 0:
                state = 0
                timer = 100
        else:
            pass



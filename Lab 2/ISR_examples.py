

# Timing
import utime
utime.sleep (0.5)       # Sleep for 0.5 sec, blocking tasks (bad)
utime.sleep_us (10)     # Sleep for only 10 us, which is usually OK
# The following measures how long a function takes to run in microseconds
start_time = utime.ticks_us ()
run_my_function ()
duration = utime.ticks_diff (utime.ticks_us, start_time)


# GPIO Pins
# Set up a pin as an output and control it
pinC0 = pyb.Pin (pyb.Pin.board.PC0, pyb.Pin.OUT_PP)
pinC0.high ()
pinC0.low ()
# Set up another pin as an input and read it
pinC1 = pyb.Pin (pyb.Pin.board.PC1, pyb.Pin.IN)
if pinC1.value ():
    print ('Pin C1 is high. Is that legal now?')




# External Interrupts
isr_count = 0
def count_isr (which_pin):        # Create an interrupt service routine
    global isr_count
    isr_count += 1
extint = pyb.ExtInt (pyb.Pin.board.PC0,   # Which pin
             pyb.ExtInt.IRQ_RISING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine
# After some events have happened on the pin...
print (isr_count)



# Timers and timer interrupts
timmy = pyb.Timer (1, prescaler=800, period=65535)             # Timer 1 running at 1000 Hz
timmy.counter ()                               # Get timer value
timer.freq (100)                               # Change rollover rate
# Define a function that toggles the pin and set it as the interrupt
# service routine. Uses pinC0 from above
def toggler (which_timer):
    if pinC0.value ():
        pinC0.low ()
    else:
        pinC0.high ()
timmy.callback (toggler)
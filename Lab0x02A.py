'''@file        Lab0x02A.py
@brief          Test reaction time by button press, timer, and LED.
@details        This is part A of the assignment and its purpose is to capture the
                reaction time of the user. The program has three main states: S0 the initializing/welcoming stage,
                S1 where the program turns the LED off and on via the utime module, and S2 where the user 
                presses the interrupt button and captures the time difference from start to button press.
                All attempts are appended to a list and averaged out for the user.
                
                Here is a snapshot of my transition diagram:
                    *  \image html Reaction_Diagram.png width=800cm height=600cm
                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab2/Lab0x02A.py

@author         David Hernandez 
@author         Sydney Lewis
@date           4/23/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''
import utime
import pyb
from pyb import Pin

# Initiates the variables at 0
myVar = 0
## @brief   Acts as a counter when buttons is pressed
#
state = 0
## @brief   Initiates the state counter at 0
#
start = 0
## @brief   Initiates tick counter at 0
#
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
## @brief   Locates blue button on nucleo
#  @details Associates further commands with this button
#
pinLED = Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## @brief   Locates LED on nucleo.
#  @details Associates further commands with this LED.
#
reactionTime = []
## @brief    List that collects reaction time to be averaged.
#  @details  Will append the reaction times for each button press.
#

def onButtonPressFCN(IRQ_src):
    '''@brief   Commands the button to capture reaction times.
       @details Callback button to capture the user reaction time on each press.
       @param   IRQ_src the command to give the button
                myVar counts amount button is press
                start counts the microseconds that pass
                since button was pushed.
    '''
    global myVar
    myVar += 1
    
    
    global start
    start = utime.ticks_us()

ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
## @brief    Sets the callback
#  @details  The command interrupts current code and calls back function
#            adding a counter each time
#

def comp_average(num):
    ''' @brief      This function computes the average of a list of values
        @details    It takes the length of the appended list for reaction times,
                    sums them up, and divides by the total count (sum_num) of 
                    presses.
        @param      num is the list input or reactionTime list for this program.
    '''
    sum_num = 0
    for t in num:
        sum_num = sum_num + t

        avg = sum_num / len(num)
    return avg


if __name__=="__main__":
    '''@brief   Initiates program when script is executed only.
    '''
    # Program initialization goes here
    state = 0   # Initial state is the init state
    print (' Welcome! Please press the BLUE BUTTON to get started. When the GREEN LED' 
            ' turns on to test your reaction time by pressing the BLUE BUTTON again (ctrl+c at any time to quit):')

    while True:
        try:

             tim2 = pyb.Timer(2, freq = 20000)
             ## @brief Sets timer channel for LED on nucleo
             #
             t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin = pinLED)
             ## @brief Control pulse width percent of LED
             #

             ## Initializes the program with welcome message
             if state == 0:
                 # On button press it will add 1 to the counter and transition to state 1
                 if myVar == 1:
                     state = 1
                     print('Standby for LED')

             ## Turns LED on for 1 second after 2 seconds have elapsed. Continues until user presses button
             elif state == 1:
                     # Sets a time limit for led to be on and off
                     if utime.ticks_diff(utime.ticks_us(),start) < 2000000:
                         t2ch1.pulse_width_percent(0)
                     elif utime.ticks_diff(utime.ticks_us(),start)  >= 2000000 and utime.ticks_diff(utime.ticks_us(),start) <= 3000000:
                         t2ch1.pulse_width_percent(100)   
                     elif utime.ticks_diff(utime.ticks_us(),start)  > 3000000:
                     # Reset timer for next transition
                         start = utime.ticks_us()

                     # Transitions the led from state 1 to state 2
                     # Accounts time when button is pressed
                     if myVar == 2:
                         current = utime.ticks_us()
                         state = 2

            ## Computes the delta time from start to button press
            #  and returns to state 1
             elif state == 2: 
                    elapsedTime = utime.ticks_diff(current,start)
                    ## @brief      Computes the delta time from start to button press
                    #  @details    When the program initiates the start counter begins
                    #              when the user presses the button when the LED is on
                    #              it takes the difference between the two points in
                    #              time, also known as the delta time.
                    #
                    reactionTime.append(elapsedTime)
                    print('Time Elapsed from start to button press:', elapsedTime,'[microseconds]')
                    print('Reaction Time:',reactionTime)
                    myVar = 1
                    state = 1

             else:
                print('Invalid input, goodbye!')
                break
            # code to run if state is invalid
            # program should ideally never reach here
            
           
        except KeyboardInterrupt:
                # This except block catches "Ctrl+C" from the keyboard
                # while (True) loop when desired
                if len(reactionTime) == 0:
                    print('ERROR: NO reaction time measured')
                else:
                    print("The average reaction time is", comp_average(reactionTime), 'microseconds')

                break
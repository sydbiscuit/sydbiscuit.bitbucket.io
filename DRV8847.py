'''@file        DRV8847.py
@brief          This file contains the class DRV8847 to control the platform motors.
@details        This class allows the user to configure the pins, timer, and channel
                to an H-Bridge motor. The user can then create motor objects to test
                any motor.

                See source code here:
                    https://bitbucket.org/dhern138/me305_lab/src/syd/ME405_Labs/Lab0xFF/DRV8847.py

@author         Sydney Lewis
@author         David Hernandez
@date           06/04/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''

import pyb
from pyb import Pin, Timer
import utime



class DRV8847:
    ''' 
    @brief      A motor driver class that allows motor objects to be created.
    @details    This class uses four pin objects, two for the timer channel variables,
                two for each motor configured and a timer object. Using these it is 
                possible to configure a sleep pin and a fault pin. The driver software
                ensures the given duty command cannot exceed +/- 100, otherwise it reads the 
                input PWM percentage and sends it to the correct channel to control motor direction. 
                The driver also has the ability to disable motor motion if a fault occurs.
    '''

    def __init__ (self, nSLEEP_pin=Pin(Pin.cpu.A15, Pin.OUT_PP), nFAULT_pin=Pin(Pin.board.PB2, Pin.IN), debug=True, primary =False):
        '''
        @brief              Initializes the motor
        @param nSLEEP_pin   A Pin object to turn the motor on/off.
        @param nFAULT_pin   A Pin object to signal if the motor has encountered a fault.
        '''
        self.nFAULT_pin = nFAULT_pin
        ## @brief        Locates fault pin on H-Bridge. Allows clearance of faults.
        #
        
        self.primary = primary
        #if self.primary == True:
            ## Motor fault ISR object
            #self.mot_fault_flag = pyb.ExtInt(self.nFAULT_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.fault_CB)

        ## Sleep pin object
        self.nSLEEP_pin = nSLEEP_pin
        ## @brief        Locates sleep pin on board to enable and disable motor
        #
        ## Fault pin object
        
        self.debug = debug
        if self.debug == True:
            print ('Motor Driver Created')
        pass
        
    def enable (self):
        ''' @ brief     Turns on motor for operation
        '''
        self.nSLEEP_pin.high()
        utime.sleep_us(100)
        
        if self.debug == True:
            print ('Enabling Motor') 
        pass
    
    def disable (self):
        ''' @brief  Turns off motor
        '''
        self.nSLEEP_pin.low()
        
        if self.debug == True:
            print('')
            print ('Disabling Motor')
        pass
    
    def fault_CB(self, IRQ_src):
        '''
        @brief     A callback method to stop the motor in case of a over-current or fault condition.
        @details   This method will disable the motors and prevent damage from occuring.
        '''
        self.disable()
        self.mot_fault_flag = True
        pass
        
    def clear_fault(self):
        '''
        @brief       This method checks the motor driver for any failures and clears them.
        @details     This method checks if the motor is trying to be enabled by
                     any means and sends a True fault. This requires the user to
                     check the nfault pin and clear any faults to unlock the driver.
                     To prevent the software latch from sticking, the fault ISR
                     is disabled to allow the motor to power on without triggering
                     another momentary fault.
        '''
        
        if self.fault_pin.value() == 1:
            self.mot_fault_flag = False
            print('The fault has been cleared')
            self.enable()
        else:
            print('')
            print('The motor has encountered a fault. Clear the problem and press enter to continue.')
            input('') 
            self.enable()
        pass
    
    
    def channel (self, INx_pin, INy_pin, INxy_timer=Timer(3,freq=20000)):
        self.INxy_timer = INxy_timer
        self.INx_pin = INx_pin
        self.INy_pin = INy_pin
        return DRV8847_channel(INx_pin, INy_pin, INxy_timer)
        
            
 
class DRV8847_channel:
    def __init__ (self, INx_pin, INy_pin, INxy_timer=Timer(3,freq=20000)):
        self.INxy_timer = INxy_timer
        self.INx_pin = INx_pin
        self.INy_pin = INy_pin
        if self.INx_pin == Pin(pyb.Pin.cpu.B4):
            self.tch1 = self.INxy_timer.channel(1, pyb.Timer.PWM, pin=self.INx_pin)
            ## @brief       Sets up generic timer input to be called on by a task
            #  @details     Creates channel object for a first motor by configuring
            #               the pins and timer to be driven via PWM in half bridge 21.
            #
            self.tch2 = self.INxy_timer.channel(2, pyb.Timer.PWM, pin=self.INy_pin)
        elif self.INx_pin == Pin(pyb.Pin.cpu.B0):
            self.tch1 = self.INxy_timer.channel(3, pyb.Timer.PWM, pin=self.INx_pin)
            self.tch2 = self.INxy_timer.channel(4, pyb.Timer.PWM, pin=self.INy_pin)
        else:
            if self.debug == True:
                print('')
                print ('No channels configured for that pin.')
        pass
            
   
    def set_duty (self, duty):
        ''' @brief      This method sets the motor duty cycle.
            @ details   Positive values cause forward spin, negative values 
                        generate spin in the opposite direction.
            @param      duty; A signed integer holding the duty cycle of the 
                        PWM signal sent to the motor.
        '''
        self.duty = duty   
        if self.duty > 100:                             # Ensures the user cannot enter out of range PWM values
            self.duty = 100
            self.tch1.pulse_width_percent(self.duty)
        elif self.duty < -100:
            self.duty = -100
            self.tch2.pulse_width_percent(-1*self.duty)
        elif duty < -1:
            self.tch2.pulse_width_percent(-1*self.duty)
        else:
            self.tch1.pulse_width_percent(self.duty)
            
    def set_time(self, Time):
        ''' @brief     This method sets the time period to test the motor.
            @details   The user can set a test period from 1-10s.
            @param     Time; A user entered integer from 1-10 that sets the interval of 
                       testing time for motor operation.
        '''
        if Time > 10:
            print('Time period entered is greater than 10s, defaulting to 10s run time.') 
            self.set_duty(40)
            pyb.delay(10000)
            self.set_duty(0)
            
        elif Time < 1:
            print('Time period less than 1s, please enter a new value.')
            
        else:
            print('Time period set to {:} s\n'.format(Time))
            self.set_duty(40)
            pyb.delay(Time*1000)
            self.set_duty(0)
                                
            
    

        

        


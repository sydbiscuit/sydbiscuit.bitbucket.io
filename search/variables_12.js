var searchData=
[
  ['y_5fctr_508',['y_ctr',['../classTouchDriver_1_1TouchDriver.html#a34f7676c0a6e84f942c5fb6a4bfb36fb',1,'TouchDriver::TouchDriver']]],
  ['y_5fxy_509',['y_xy',['../classTouchDriver_1_1TouchDriver.html#aeaba2cdafb28a0f6d38278b7bb6bc4fb',1,'TouchDriver::TouchDriver']]],
  ['yavg_510',['yavg',['../classTouchDriver_1_1TouchDriver.html#a5ea0b866873c457027b6de4aa6c9caf3',1,'TouchDriver::TouchDriver']]],
  ['yavg_5fbuf_511',['yavg_buf',['../classTouchDriver_1_1TouchDriver.html#a38cd1452e43bd4c8e107472b4b4d112e',1,'TouchDriver::TouchDriver']]],
  ['yaw_5flsb_512',['yaw_LSB',['../classBNO055_1_1bno055.html#a4cd94b16d0006c77305924c0b48c4af1',1,'BNO055::bno055']]],
  ['yfilt_5fbuf_513',['yfilt_buf',['../classTouchDriver_1_1TouchDriver.html#a1f2ddc13a3eeecbacbb0675b2ba25e25',1,'TouchDriver::TouchDriver']]],
  ['ym_514',['ym',['../classTouchDriver_1_1TouchDriver.html#a37563134ee4a571827b3ceab6388f509',1,'TouchDriver::TouchDriver']]],
  ['yp_515',['yp',['../classTouchDriver_1_1TouchDriver.html#a62d7de472b167f08cd66899753297254',1,'TouchDriver::TouchDriver']]]
];

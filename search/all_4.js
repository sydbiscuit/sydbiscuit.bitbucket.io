var searchData=
[
  ['data_37',['data',['../Lab0x04__main_8py.html#a2cbc0186ac257dd8868482a644f94a41',1,'Lab0x04_main']]],
  ['data_5fl_38',['data_l',['../Lab0x04__main_8py.html#acc020235234f6fe97123d9ce4f09be9a',1,'Lab0x04_main']]],
  ['data_5fu_39',['data_u',['../Lab0x04__main_8py.html#a8725111f026bfa248acec37c9273ce99',1,'Lab0x04_main']]],
  ['datacollect_40',['dataCollect',['../Lab0x03__UI_8py.html#a0a1b53710ad2fd7792854f4fb24872d0',1,'Lab0x03_UI.dataCollect()'],['../UI__front_8py.html#a1b1421f1a05d0b3f0bc1d7df2faa4818',1,'UI_front.dataCollect()']]],
  ['datacollecttask_41',['dataCollectTask',['../dataCollectTask_8py.html#a49259461139a45ad35b98c5657277088',1,'dataCollectTask']]],
  ['datacollecttask_2epy_42',['dataCollectTask.py',['../dataCollectTask_8py.html',1,'']]],
  ['datastrip_43',['dataStrip',['../Lab0x03__UI_8py.html#a30776e2297ff22d5c64c6013382d4b17',1,'Lab0x03_UI.dataStrip()'],['../UI__front_8py.html#a1e188bb1e7521c018e23b95c80b87f2d',1,'UI_front.dataStrip()']]],
  ['debug_44',['debug',['../classDRV8847_1_1DRV8847.html#ac9d4d0dceec1a36fb6ab8079790aec80',1,'DRV8847.DRV8847.debug()'],['../classTouchDriver_1_1TouchDriver.html#a0aec20f73d61334b48b34145c72cf54b',1,'TouchDriver.TouchDriver.debug()']]],
  ['delta_45',['delta',['../classencoderDriver_1_1encoderDriver.html#a9f3bd00fb659a6f43c4b0efda1794c6c',1,'encoderDriver::encoderDriver']]],
  ['denoms_46',['denoms',['../namespaceLab0x01.html#ade97675b645200bf217bc9d494aa1698',1,'Lab0x01']]],
  ['disable_47',['disable',['../classDRV8847_1_1DRV8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable()'],['../classMotorDriver_1_1MotorDriver.html#abb9a67928c8ed29dd06b04727813411a',1,'MotorDriver.MotorDriver.disable()']]],
  ['drinkdict_48',['drinkDict',['../namespaceLab0x01.html#a25a40fd8b6778dbe0c30798f56bbe5e5',1,'Lab0x01.drinkDict()'],['../vendorLab0x01_8py.html#a9f539c8bf4f5f90061c9d6b5dac365fc',1,'vendorLab0x01.drinkDict()']]],
  ['drv8847_49',['DRV8847',['../classDRV8847_1_1DRV8847.html',1,'DRV8847']]],
  ['drv8847_2epy_50',['DRV8847.py',['../DRV8847_8py.html',1,'']]],
  ['drv8847_5fchannel_51',['DRV8847_channel',['../classDRV8847_1_1DRV8847__channel.html',1,'DRV8847']]]
];

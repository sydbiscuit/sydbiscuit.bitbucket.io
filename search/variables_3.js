var searchData=
[
  ['data_391',['data',['../Lab0x04__main_8py.html#a2cbc0186ac257dd8868482a644f94a41',1,'Lab0x04_main']]],
  ['data_5fl_392',['data_l',['../Lab0x04__main_8py.html#acc020235234f6fe97123d9ce4f09be9a',1,'Lab0x04_main']]],
  ['data_5fu_393',['data_u',['../Lab0x04__main_8py.html#a8725111f026bfa248acec37c9273ce99',1,'Lab0x04_main']]],
  ['datacollect_394',['dataCollect',['../Lab0x03__UI_8py.html#a0a1b53710ad2fd7792854f4fb24872d0',1,'Lab0x03_UI.dataCollect()'],['../UI__front_8py.html#a1b1421f1a05d0b3f0bc1d7df2faa4818',1,'UI_front.dataCollect()']]],
  ['datastrip_395',['dataStrip',['../Lab0x03__UI_8py.html#a30776e2297ff22d5c64c6013382d4b17',1,'Lab0x03_UI.dataStrip()'],['../UI__front_8py.html#a1e188bb1e7521c018e23b95c80b87f2d',1,'UI_front.dataStrip()']]],
  ['debug_396',['debug',['../classDRV8847_1_1DRV8847.html#ac9d4d0dceec1a36fb6ab8079790aec80',1,'DRV8847.DRV8847.debug()'],['../classTouchDriver_1_1TouchDriver.html#a0aec20f73d61334b48b34145c72cf54b',1,'TouchDriver.TouchDriver.debug()']]],
  ['delta_397',['delta',['../classencoderDriver_1_1encoderDriver.html#a9f3bd00fb659a6f43c4b0efda1794c6c',1,'encoderDriver::encoderDriver']]],
  ['denoms_398',['denoms',['../namespaceLab0x01.html#ade97675b645200bf217bc9d494aa1698',1,'Lab0x01']]],
  ['drinkdict_399',['drinkDict',['../namespaceLab0x01.html#a25a40fd8b6778dbe0c30798f56bbe5e5',1,'Lab0x01.drinkDict()'],['../vendorLab0x01_8py.html#a9f539c8bf4f5f90061c9d6b5dac365fc',1,'vendorLab0x01.drinkDict()']]]
];

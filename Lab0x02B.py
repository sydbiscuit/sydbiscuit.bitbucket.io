'''@file        Lab0x02B.py
@brief          Test reaction time by button press, timer, and LED.
@details        This is part B of the assignment and its purpose is to capture the
                reaction time of the user. The program has three main states: S0 the initializing/welcoming stage,
                S1 where the program turns the LED off and on via the output compare function and timer set up, and S2 where the user 
                presses the blue button and the input capture function captures the time difference from start to button press.
                All attempts are appended to a list and averaged out for the user.

                Here is a snapshot of our transition diagram:
                    *  \image html Reaction2_Diagram.png width=800cm height=600cm
                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab2/Lab0x02B.py

@author         David Hernandez
@author         Sydney Lewis
@date           4/28/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''


import pyb
from pyb import Pin, Timer

# Initiates the variables at 0
state = 0
## @brief   Initiates the state counter at 0
#
_substate = 0
## @brief    Used to create mini-FSM within the larger FSM
#  @details  Commands what to do when user presses the button
#
reactionTime = []
## @brief    List that collects reaction time to be averaged.
#  @details  Will append the reaction times for each button press.
#
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
## @brief   Locates blue button on nucleo
#  @details Associates further commands with this button
#
pinLED = Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## @brief   Locates LED on nucleo
#  @details Associates further commands with this LED
#
PB3 = pyb.Pin (pyb.Pin.cpu.B3)
## @brief   Locates pin B3 on the nucleo board
#  @details Will trigger when PC13 is pressed
#           and assist in input capture.
#
last_OC = 0
## @brief   First time compare match
#  @details Compare matches with timer and updates
#           the new compare match at designated rate.
#           Triggers the LED on and off.
#
elapsedTime = None
## @brief   Initialized the reaction time
#

def OC_Callback(tim2):
    '''@brief   Callback function to compare match to LED.
       @details Callback button to turn LED on and off when localized
                interrupt occurs. Compares given value to internal timer value
                and turns LED on for one second, and then off until compare
                match happens again.
       @param   tim2 the command to match on the tim2 counter timeline
    '''
    # Compares to last compare match and updates
    global last_OC
    
    per = 65535    # Max period of the timer
    
    last_OC = t2ch1.compare()
    last_OC += 1000
    # Prevents overflow
    last_OC &= per
    t2ch1.compare(last_OC)
    

def IC_Callback(timer):
    '''@brief   Callback function that captures time on button press.
       @details Callback button that user will press when the LED is high.
                This will store all user reaction times as necessary.
       @param   timer the command to give the button
                elapsed time multplies count by delta time
                since button was pushed.
    '''
    global elapsedTime
    captureTime = t2ch2.capture()
    
    # The 50 comes from dT = 12.5*prescaler, which prescaler is 4000 here
    # Then convereted from nano to microsecond

    if pinLED.value():
        elapsedTime = last_OC*50 - captureTime
        # @brief   The 50 comes from dT = 12.5*prescaler, which prescaler is 4000 here
        #          then convereted from nano to microsecond.
        print('Time Elapsed from start to button press:', elapsedTime,'[microseconds]')
        captureTime = 0

tim2 = Timer(2, period=0xFFFF, prescaler=4000)
## @brief      Selects timer on Nucleo board to use for program.
#  @details    The timer selected on board is arbitrarily 2.
#              The period is the largest allowed for a 16-bit (65536).
#              The prescaler returned a convenient delta time to work with 
#              50 microseconds and a time period of the "stairs" of 3.277 [s].
#              The frequency is reasonable for LED operations.
#
## Calls on compare match function
#  Triggers the LED on and off on a match
t2ch1 = tim2.channel(1, Timer.OC_TOGGLE, pin=pinLED, callback=OC_Callback)
## Call on input capture function
#  Recognizes blue button press and records timestamp
t2ch2 = tim2.channel(2, Timer.IC, polarity=Timer.FALLING, pin=PB3, callback=IC_Callback)

def comp_average(num):
    ''' @brief      This function computes the average of a list of values
        @details    It takes the length of the appended list for reaction times,
                    sums them up, and divides by the total count (sum_num) of 
                    presses.
        @param      num is the list input or reactionTime list for this program.
    '''
    sum_num = 0
    for t in num:
        sum_num = sum_num + t

        avg = sum_num / len(num)
    return avg


if __name__=="__main__":
    '''@brief      Initiates the reaction test program.
       @details    Uses three states S0-Initialize, S1-LED Toggle, and S2-Reaction time capture.
    '''
    # Program initialization goes here
    state = 0   # Initial state is the init state
    print (' Welcome! When the GREEN LED turns ON to press the BLUE BUTTON to test your reaction time' 
            '  (ctrl+c at any time to quit):')

    while True:
        try:

             ## Initializes the program with welcome message
             if state == 0:
                 # On button press it will add 1 to the counter and transition to state 1
                     state = 1
                     print('Standby for LED')

             ## Turns LED on for 1 second after 2 seconds have elapsed. Continues until user presses button
             elif state == 1:
                     # Transitions the led from state 1 to state 2
                     # Accounts time when button is pressed
                     state = 2

            ## Mini FSM. Transitions according to users button press reaction
            #  If user presses on LED on, then it will append the data to the array
            #  If user presses on LED off, then it will return to toggling.
             elif state == 2:
                 if _substate == 1:
                     if pinLED.value():
                         _substate == 2
                 if _substate == 2:
                     if pinLED.value() == False:
                         _substate == 1
                 
                 ## This portion appends the input capture if pressed on LED on
                 #  Appended to reaction array after being corrected
                 #  and put into ms for quality of life.
                 if elapsedTime != None:
                     reactionTime.append((elapsedTime-last_OC)/1000)
                     print('Reaction Time:',reactionTime,'milliseconds')
                     elapsedTime = None
                     state = 0

             else:
                print('Invalid input, goodbye!')
                break
            # code to run if state is invalid
            # program should ideally never reach here
            
           
        except KeyboardInterrupt:
                # This except block catches "Ctrl+C" from the keyboard
                # while (True) loop when desired
                if len(reactionTime) == 0:
                    print('ERROR: NO reaction time measured')
                else:
                    print("Your AVERAGE reaction time is", comp_average(reactionTime), 'milliseconds')

                break
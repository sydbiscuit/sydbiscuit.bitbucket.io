'''@file        bnoTask.py
@brief          This program is the task for the BNO055 IMU.
@details        When combined with the BNO055 driver, the IMU will be able to use inertial measurement
                to provide enhanced feedback and error checking during balancing operation.
                
                
                See source code here:
                    https://bitbucket.org/dhern138/me305_lab/src/syd/ME405_Labs/Lab0xFF/bnoTask.py

@author         Sydney Lewis
@author         David Hernandez
@date           06/08/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''
import pyb
from pyb import I2C
import BNO055
from BNO055 import bno055
import utime
import shares


def bnoTask():
    print('getting state')
    state = shares.bnoState.get() 
    if state == 0:
        print('init')
        i2c = I2C(1)
        i2c.init(I2C.MASTER, baudrate=115200, gencall=False, dma=False)
        utime.sleep_ms(100)
        print('setting up IMU')
        imu = bno055(i2c)
        bno055.Euler(imu)
        print(shares.pitch_Euler.get())
        
        # imu = bno055(i2c)
        # bno055.check(imu)
        # if True:
        #     print('test confirmed')
        #     yield(state)
        # else:
        #     print('No device found')
        #     yield(state)
    else:
    #elif state == 1:
        #bno055.Euler()
        print('done')
        yield(state)
    # else:
    #     print('this sux')
    #     yield(state)
              
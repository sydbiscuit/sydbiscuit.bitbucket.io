var Lab0x04__main_8py =
[
    [ "MCP9808", "classLab0x04__main_1_1MCP9808.html", "classLab0x04__main_1_1MCP9808" ],
    [ "adcall", "Lab0x04__main_8py.html#a4c6c8cfa670b3c206592118643ec1e21", null ],
    [ "baudrate", "Lab0x04__main_8py.html#ac2071c335600ad79f66f780874443f1e", null ],
    [ "data", "Lab0x04__main_8py.html#a2cbc0186ac257dd8868482a644f94a41", null ],
    [ "data_l", "Lab0x04__main_8py.html#acc020235234f6fe97123d9ce4f09be9a", null ],
    [ "data_u", "Lab0x04__main_8py.html#a8725111f026bfa248acec37c9273ce99", null ],
    [ "dma", "Lab0x04__main_8py.html#a57deba694c0815bf71ac13e302c152ba", null ],
    [ "False", "Lab0x04__main_8py.html#a2eeb365a1e5706cf7e1131bd9e894b28", null ],
    [ "gencall", "Lab0x04__main_8py.html#aef2694fb69f7267c31c2684d49fc6ef9", null ],
    [ "i2c_1", "Lab0x04__main_8py.html#a9725858f69458bcda86a51ccc42e18fe", null ],
    [ "ID", "Lab0x04__main_8py.html#ade2ca7e516476e1a896be2e92fa0cbf5", null ],
    [ "MASTER", "Lab0x04__main_8py.html#a8f4cc5683ba7bc860cc07fd9f73c07da", null ],
    [ "mc_F", "Lab0x04__main_8py.html#a93173cbbc520cd71e1e5de16e01847e1", null ],
    [ "period", "Lab0x04__main_8py.html#a00fe864a46e7f0c30f5e259421f7d5b1", null ],
    [ "sensor", "Lab0x04__main_8py.html#afbe45c6378414f980bc525a913bbf452", null ],
    [ "state", "Lab0x04__main_8py.html#a025307ed3f9943bf2f57f2e073a66eda", null ],
    [ "temp_mc", "Lab0x04__main_8py.html#a28d1920dd4f223712264881b57f1ca13", null ],
    [ "temp_sens", "Lab0x04__main_8py.html#a893dfa93d7f8e0e0bf69e1202e3790e8", null ],
    [ "Tf", "Lab0x04__main_8py.html#a918afee2bc4bf54231239b5036deabd6", null ],
    [ "time", "Lab0x04__main_8py.html#a2d4ffeb0bc6d19b27b40886fe1afac2a", null ],
    [ "val", "Lab0x04__main_8py.html#ad338c0f8c2644498e49a40dc841e7ba2", null ]
];
## \page page1 Lab0x01
#  \tableofcontents
#  The following document used the Python language to develop a program that would simulate the functions
#  of a common vending machine. This included all the complex functions of a typical vending maching such as:
#  Displaying vending nomenclature with correlated price, Accepting customer inputs of monetary type or beverage selection,
#  complex calculations of balance, and finally a responsive eject function that returned all available funds.
#  \section sec1 Vendotron: The Simulated Vending Machine
#  This page contains the subsections \ref Lab0x01.py
#  \subsection subsection1 The Incredible Lab0x01.py
#  As mentioned in the introduction, this file performs the simulated functions of common vending machine one would
#  find out near a school or job site. The intention was to use a generator type function, which would continuously 
#  run the tasks within the function. This made it possible to continuously accept customer funds and or beverage
#  selections. The programs flows very well between the 5 possible states. It uses a 2 functions outside of the
#  generator function, one to calculate the difference in price and funds and the other which accepts user inputs.
#  It transitions smoothly according to the transition diagram provided below. 
#  The plot image of the transition diagram can be viewed in this file below.
#     \image html FSM_Lab1.png "Figure 1. Finite State Machine for Vendotron" width=800cm height=600cm
#
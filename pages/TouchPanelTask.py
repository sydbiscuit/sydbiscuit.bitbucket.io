'''@file        TouchPanelTask.py
@brief          This program creates a resistive touch panel (RTP) object from the developed driver.
@details        This task allows the user to create an RTP object from the driver class TouchDriver developed
                beforehand. The RTP object can be used to test, calibrate, and gather required positional
                information from the x and y axes. This data is crucial for the overall system model.

                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/TouchPanelTask.py

@author         Sydney Lewis
@author         David Hernandez
@date           5/13/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''

from TouchDriver import TouchPanel
from pyb import Pin
import utime

PIN_ym = Pin(Pin.cpu.A0)
## @brief    Associates ym to pinA0 on NucleoSTM32
#
PIN_xm = Pin(Pin.cpu.A1)
## @brief    Associates xm to pinA1 on NucleoSTM32
#
PIN_yp = Pin(Pin.cpu.A6)
## @brief    Associates yp to pinA6 on NucleoSTM32
#
PIN_xp = Pin(Pin.cpu.A7)
## @brief    Associates xp to pinA7 on NucleoSTM32
#


if __name__ == '__main__':
    ## The following code allows user to interact with touch panel.
    #

    TouchPanel = TouchPanel(PIN_ym,PIN_xm,PIN_yp,PIN_xp,(176,100),(88,50),debug=False)
    ##@brief    The touch panel object created to work touchdriver class.
    # @details  Calls on the touch driver class to input arguments into the attributes
    #           which will be used within the class driver code. Each parameter is discussed
    #           in the driver. Please look at driver code for more information on each param.
    #

    while True:

        sel_test = input('Running or point test? (r/p): ')
        ## @brief    Allows user to select whether to test points or while running
        #
        sel_axis = input('Which axis would you like to test (X Y Z ALL): ') 
        ## @brief    Allows user to select which axis or axes to test.
        #
        try:
            
            if sel_test == 'p':
                if sel_axis == 'X' or sel_axis == 'x':
                    while True:
                        start = utime.ticks_us()
                        x = TouchPanel.scan_x()
                        ##@brief    Calls on touch driver class and its method.
                        #
                        stop = utime.ticks_us()
                        ## Test run time in us
                        run_time = utime.ticks_diff(stop,start)
                        print('Contact coordinate X = '+str(x)+' mm in '+str(run_time)+' us\n')
                        input('     Press enter to test again: ')
                        
                elif sel_axis == 'y' or sel_axis == 'Y':
                    while True:
                        start = utime.ticks_us()
                        y = TouchPanel.scan_y()
                        ##@brief    Calls on touch driver class and its method.
                        #
                        stop = utime.ticks_us()
                        run_time = utime.ticks_diff(stop,start)
                        print('Contact coordinate Y = '+str(y)+' mm in '+str(run_time)+' us\n')
                        input('     Press enter to test again: ')
                        
                elif sel_axis == 'Z' or sel_axis == 'z':
                    while True:
                        start = utime.ticks_us()
                        z = TouchPanel.scan_z()
                        ##@brief    Calls on touch driver class and its method.
                        #
                        stop = utime.ticks_us()
                        run_time = utime.ticks_diff(stop,start)
                        print('Contact Test Z = '+str(z)+' mm in '+str(run_time)+' us\n')
                        input('     Press enter to test again: ')
                        
                else:
                     while True:
                        start = utime.ticks_us()
                        pos = TouchPanel.scan()
                        ##@brief    Calls on touch driver class and its method.
                        # @details  This allows user to test and observe the
                        #           three components at once.
                        #
                        stop = utime.ticks_us()
                        run_time = utime.ticks_diff(stop,start)
                        print('Contact coordinates ({:},{:},{:}) [mm] in {:} us\n'.format(pos[0],pos[1],pos[2],run_time))
                        input('     Press enter to test again: ')
                    
            else:
                if sel_axis == 'X' or sel_axis == 'x':
                    print('x-axis test')
                    while True:
                        x = TouchPanel.scan_x(mode='ctr')
                        print('X: {:} mm'.format(x))
                        utime.sleep_ms(250)
                        
                        
                elif sel_axis == 'y' or sel_axis == 'Y':
                    print('y-axis test')
                    while True:
                        y = TouchPanel.scan_y(mode='ctr')
                        print('Y: {:} mm'.format(y))
                        utime.sleep_ms(250)
                        
                elif sel_axis == 'Z' or sel_axis == 'z':
                    print('Contact test')
                    while True:
                        z = TouchPanel.scan_z()
                        print('Z: {:} mm'.format(z))
                        
                else:
                    print('Selected All Axes')
                    while True:
                       pos = TouchPanel.scan(mode='ctr')
                       print('Contact coordinates ({:},{:},{:}) [mm]'.format(pos[0],pos[1],pos[2]))
                       utime.sleep_ms(250)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed, program ended. Goodbye')
            break